#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooPolynomial.h"
#include "RooHistPdf.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"
#include <cmath>

using namespace RooFit;
 
// to keep together the names and plot properties for each template histogram
struct templatePlotDescription{
    std::string name;
    Int_t color;

    std::string plotName;
    std::string histPdfName;
    std::string normHistPdfName;
    std::string extHistPdfName;
    std::string histConstrName;
    std::string dataHistName;

    templatePlotDescription(std::string n, Int_t c) : name(n), color(c){
        plotName        = name;
        histPdfName     = "rooHistPdf_" + name;
        histConstrName  = "rooHistConstr_" + name;
        normHistPdfName = "n_" + name;
        extHistPdfName  = "ext_" + histPdfName;
        dataHistName    = "rooDataHist_" + name;
    }

};


// to keep together the pdf and stat constraint or each template
struct templateFitObj{
   RooParamHistFunc*     histPdf;
   RooHistConstraint*    histConstraint;
};

struct templateHistogram{
    std::string name;
    RooDataHist* histData;
    RooHistPdf*  histPdf;
};


std::vector<templateHistogram> makeTemplateHistograms(const std::string& rootFileName, std::vector<templatePlotDescription> fitTemplates, RooRealVar* histVar, Int_t rebin_factor) {
    
    std::vector<templateHistogram> templates;
    // std::vector<RooDataHist*> dataHists;
    // Open the ROOT file
    TFile* file = TFile::Open(rootFileName.c_str());

    // Loop over the list of histogram names
    for (const auto& fTemplate : fitTemplates) {
        // Get the histogram from the ROOT file
        std::string histName = fTemplate.name;
        TH1* hist = dynamic_cast<TH1*>(file->Get(histName.c_str()));
        hist->Rebin(rebin_factor);
        hist->Sumw2();
        

        // Check if the histogram is found in the file, skip this tmplt otherwise
        if (!hist) {
            std::cerr << "Error: Histogram '" << fTemplate.name << "' not found in ROOT file." << std::endl;
            continue;
        }

        // Create a RooDataHist from the TH1
        RooDataHist* rooDataHist = new RooDataHist(fTemplate.dataHistName.c_str(), "", *histVar, Import(*hist));
        // Create a RooHistPdf from the RooDataHist
        RooHistPdf* rooHistPdf = new RooHistPdf(fTemplate.histPdfName.c_str(), fTemplate.histPdfName.c_str(), *histVar, *rooDataHist);

        templateHistogram tmplt;
        tmplt.name = fTemplate.name;
        tmplt.histData = rooDataHist;
        tmplt.histPdf = rooHistPdf;

        // dataHists.push_back(rooDataHist);
        templates.push_back(tmplt);
        

    }
    // Clean up
    file->Close();

    // return dataHists;
    return templates;
}

// make rigid MC templates without stat unc-ty
std::vector<RooHistPdf*> createRooHistPdfs(const std::string& rootFileName, std::vector<templatePlotDescription> fitTemplates, RooRealVar& histVar, Int_t rebin_factor) {
    
    std::vector<RooHistPdf*> histPdfs;
    std::vector<RooDataHist*> dataHists;
    // Open the ROOT file
    TFile* file = TFile::Open(rootFileName.c_str());

    // Loop over the list of histogram names
    for (const auto& fTemplate : fitTemplates) {
        // Get the histogram from the ROOT file
        std::string histName = fTemplate.name;
        TH1* hist = dynamic_cast<TH1*>(file->Get(histName.c_str()));
        hist->Rebin(rebin_factor);

        // Check if the histogram is found in the file
        if (!hist) {
            std::cerr << "Error: Histogram '" << fTemplate.name << "' not found in ROOT file." << std::endl;
            continue;
        }

        // Create a RooDataHist from the TH1
        RooDataHist* rooDataHist = new RooDataHist(("rooDataHist_" + fTemplate.name).c_str(), "RooDataHist from TH1", histVar, Import(*hist));
        // Create a RooHistPdf from the RooDataHist
        RooHistPdf* rooHistPdf = new RooHistPdf(fTemplate.histPdfName.c_str(), fTemplate.histPdfName.c_str(), histVar, *rooDataHist);
        histPdfs.push_back(rooHistPdf);
        dataHists.push_back(rooDataHist);

    }
    // Clean up
    file->Close();

    return histPdfs;
}

// Implementing the Barlow-Beeston method for taking into account the statistical uncertainty of a Monte-Carlo fit template. 
std::vector<RooParamHistFunc*> createConstrainedHistPdfs(const std::string& rootFileName, std::vector<templatePlotDescription> fitTemplates, RooRealVar& histVar, Int_t rebin_factor) {
    
    std::vector<RooParamHistFunc*> histPdfs;
    std::vector<RooDataHist*> dataHists;
    // Open the ROOT file
    TFile* file = TFile::Open(rootFileName.c_str());

    // Loop over the list of histogram names
    for (const auto& fTemplate : fitTemplates) {
        // Get the histogram from the ROOT file
        std::string histName = fTemplate.name;
        TH1* hist = dynamic_cast<TH1*>(file->Get(histName.c_str()));
        hist->Rebin(rebin_factor);
        // hist->Sumw2();

        // Check if the histogram is found in the file
        if (!hist) {
            std::cerr << "Error: Histogram '" << fTemplate.name << "' not found in ROOT file." << std::endl;
            continue;
        }

        // Create a RooDataHist from the TH1
        RooDataHist* rooDataHist = new RooDataHist(("rooDataHist_" + fTemplate.name).c_str(), "RooDataHist from TH1", histVar, Import(*hist));
        // Create a RooParamHistFunc from the RooDataHist
        RooParamHistFunc* rooParamHistFunc = new RooParamHistFunc(fTemplate.histPdfName.c_str(), fTemplate.histPdfName.c_str(), histVar, *rooDataHist);

        histPdfs.push_back(rooParamHistFunc);
        dataHists.push_back(rooDataHist);

    }
    // Clean up
    file->Close();

    return histPdfs;
}



// each bin of pdf has uncty, it's broken
int fit_templates() {

    // take these from templates later!
    Double_t mass_min = 0.2;
    // Double_t mass_max = 1.4;
    Double_t mass_max = 0.6;
    Int_t rebin_factor = 1;

    RooRealVar mass("mass", "mass", mass_min, mass_max);

    // Specify the ROOT file containing data histogram and its name
    std::string dFileName = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/histoMMGFullRun3.root";
    std::string dHistName = "pfCandPhotons_minDr_massDimu_all";
    TFile* dFile = TFile::Open(dFileName.c_str());
    // Get the target histogram from the file
    TH1F* th1 = dynamic_cast<TH1F*>(dFile->Get(dHistName.c_str()));

    double binWidth = th1->GetXaxis()->GetBinWidth(1);
    th1->Rebin(rebin_factor);

    
    RooDataHist* dHist = new RooDataHist("dataHist", "RooDataHist from TH1", RooArgSet(mass), th1);
    Double_t dInt = dHist->sumEntries();
    // Specify the ROOT file containing templates and list of histogram names
    // std::string rootFileName = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/inclusiveDiLeptonMC/full/merged/template_hists/merged_templates.root"; //merged_templates_woHLT
    std::string rootFileName = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/inclusiveDiLeptonMC/full/merged/template_hists/merged_templates_woHLT.root"; //merged_templates_woHLT


    std::vector<templatePlotDescription> fitTemplates = {
        {"isEta2MuMu",              42},
        {"isEta2MuMuGamma",         8},
        // {"isEtaPrime2MuMuGamma",    45},
        {"isOmega2MuMu",            46},
        {"isOmega2Pi0MuMu",         40},
        {"isRho2MuMu",              38},
        {"isPhi2MuMu",              6},
        // {"isPhi2KK",                7},
        {"isKK2mumu",               5},
        {"isPiPi2mumu",             9},
        {"combinatorial",           29},
       
    };

    

    TCanvas *c = new TCanvas("rf706_histpdf", "rf706_histpdf", 800, 400);

    // RooPlot *frame1 = mass.frame();


    // Create a RooArgList to hold the individual PDFs and normalisations
    RooArgList pdfList;
    RooArgList normList;
    // list to hold constraints
    RooArgSet constrList;

    // Create RooHistPdfs for the specified histograms
    std::vector<RooParamHistFunc*>   histPDFs =  createConstrainedHistPdfs(rootFileName, fitTemplates, mass, rebin_factor);
    
    // std::vector<RooExtendPdf*> eHistPDFs;
    std::vector<RooRealVar*>   histNorms;

    for (size_t i = 0; i < fitTemplates.size(); ++i){

        std::cout<< "####### Composing template" <<std::endl;
        std::cout<< fitTemplates[i].normHistPdfName <<std::endl;

        RooRealVar* norm = new RooRealVar(fitTemplates[i].normHistPdfName.c_str(), fitTemplates[i].normHistPdfName.c_str(), 0, dInt);
        norm->setVal(1e3);
        histNorms.push_back(norm);
        pdfList.add(*histPDFs[i]);
        normList.add(*norm);
    
    }

    // // Create the sum PDF using RooAddPdf
    // RooAddPdf sumPdf("sumPdf", "Sum of PDFs", pdfList);

    // Construct the sum of templates
    RooRealSumPdf model_tmp("sp_ph", "sp_ph", pdfList, normList, true);

    for (size_t i = 0; i < fitTemplates.size(); ++i){
        // Constraint to limit each bin of pdf
        RooHistConstraint* histConstraint = new RooHistConstraint(fitTemplates[i].histConstrName.c_str(), fitTemplates[i].histConstrName.c_str(), *histPDFs[i]);
        constrList.add(*histConstraint);
    }
    // Construct the joint model with template PDFs and constraints
    RooProdPdf sumPdf("sumPdf","sumPdf", constrList, Conditional(model_tmp, mass));

    // Fit the sum PDF to the target histogram
    RooFitResult* fitResult = sumPdf.fitTo(*dHist, RooFit::Save(true));

    // Print fit results
    fitResult->Print();

    // Plot the result
    RooPlot* fitFrame = mass.frame();

    // Create a canvas and draw the frame
    TCanvas fitCanv("canvas", "Fit Result", 800, 600);
    TLegend *legend = new TLegend(0.7, 0.7, 0.9, 0.9);


    // Create the main pad (upper part)
    TPad* mainPad = new TPad("mainPad", "Main Pad", 0, 0.3, 1, 1);
    mainPad->SetBottomMargin(0); // Set bottom margin to zero
    mainPad->Draw();
    mainPad->cd();

    dHist->plotOn(fitFrame , RooFit::LineColor(kBlack), RooFit::Components("dataHist"), RooFit::Name("Data"), DataError(RooAbsData::Poisson));
    sumPdf.plotOn(fitFrame, RooFit::LineColor(kBlue), RooFit::Components("sumPdf"), RooFit::Name("total pdf"));
    // Construct a histogram with the pulls of the data w.r.t the curve
    // RooHist *hpull = fitFrame->pullHist();

    

    int newSignificantDigits = 1; // for the chi2 display
    for (size_t i = 0; i < fitTemplates.size(); ++i){
        // Plot the components with different colors
        // sumPdf.plotOn(fitFrame, RooFit::LineColor(fitTemplates[i].color), RooFit::Components(fitTemplates[i].extHistPdfName.c_str()), RooFit::Name(fitTemplates[i].plotName.c_str()));
        // // Add a legend
        // legend->AddEntry(fitFrame->findObject(fitTemplates[i].plotName.c_str()), fitTemplates[i].plotName.c_str(), "l");
        sumPdf.plotOn(fitFrame, RooFit::LineColor(fitTemplates[i].color), RooFit::Components(fitTemplates[i].histPdfName.c_str()), RooFit::Name(fitTemplates[i].plotName.c_str()));
        // Add a legend
        // double chi2Val = fitFrame->chiSquare(fitTemplates[i].histPdfName.c_str(), "dataHist");
        // double orderOfMagnitude = std::pow(10, std::floor(std::log10(std::abs(chi2Val))));
        // double roundedChi2Val = std::round(roundedChi2Val / orderOfMagnitude * std::pow(10, newSignificantDigits)) / std::pow(10, newSignificantDigits);
        // std::string legend_field = fitTemplates[i].plotName + " chi2= " + std::to_string(roundedChi2Val);
        std::string legend_field = fitTemplates[i].plotName;
        legend->AddEntry(fitFrame->findObject(fitTemplates[i].plotName.c_str()), legend_field.c_str(), "l");

    }
    legend->AddEntry(fitFrame->findObject("Data"), "Data", "l");
    legend->AddEntry(fitFrame->findObject("total pdf"), "total pdf", "l");
    fitFrame->Draw();
    legend->Draw();
    cout << "chi^2 = " << fitFrame->chiSquare() << endl;
    

    // Create the residuals pad (lower part)
    fitCanv.cd(); // Move back to the canvas
    TPad* residualsPad = new TPad("residualsPad", "Residuals Pad", 0, 0, 1, 0.3);
    // residualsPad->SetPad(0, 0, 1, 0.3);
    residualsPad->SetTopMargin(0); // Set top margin to zero
    residualsPad->Draw();
    residualsPad->cd();
    // hpull->Draw();

    // Create pulls
    // Get TH1 histogram from RooHistPdf
    TH1* histFromPdf = sumPdf.createHistogram("pdf hist", mass, RooFit::Binning(mass.getBins()), RooFit::Extended(true));
    TH1F *histResid = new TH1F("histResid","histResid", histFromPdf->GetNbinsX(), histFromPdf->GetXaxis()->GetXmin(), histFromPdf->GetXaxis()->GetXmax());
    for (Int_t i=1; i<=histFromPdf->GetNbinsX(); i++) {
        Double_t pull = (th1->GetBinContent(i) - histFromPdf->GetBinContent(i)) / th1->GetBinContent(i);
        histResid->SetBinContent(i,pull);
    }
    histResid->Draw();


    // hpull->GetYaxis()->SetRangeUser(-100, 100);



    
    // fitCanv.SetLogy();
    // fitCanv.Draw();
    fitCanv.SaveAs("../combine_output/plots/FitResult_withHLT.pdf");

    TCanvas fitCanv2("canvas", "Fit Result", 800, 600);
    fitFrame->Draw();
    legend->Draw();
    fitCanv2.SetLogy();
    fitCanv2.SaveAs("../combine_output/plots/FitResult_withHLT_log.pdf");

    // // sumPdf.plotOn(frame1);
    // frame1->GetYaxis()->SetTitleOffset(1.4);
    // frame1->Draw();
    // c->SaveAs("./tmp.pdf");


    // // Clean up allocated memory
    // for (const auto& histPdf : histPDFs) {
    //     delete histPdf;
    // }
    return 0;
}











// // version using roohistpdf
// int template_mc() {

//     // take these from templates later!
//     Double_t mass_min = 0.2;
//     Double_t mass_max = 1.4;
//     // Double_t mass_min = 0.2;
//     // Double_t mass_max = 1.0;
//     Int_t rebin_factor = 3;

//     RooRealVar* mass = new RooRealVar("mass", "mass", 0.2, 1.4);

//     // Specify the ROOT file containing data histogram and its name
//     std::string dFileName = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/histoMMGFullRun3.root";
//     std::string dHistName = "pfCandPhotons_minDr_massDimu_all";
//     // std::string dFileName = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/histosJan29.root";
//     // std::string dHistName = "massDimu_pt11to13_barrel_isMediumMuon";
//     TFile* dFile = TFile::Open(dFileName.c_str());
//     // Get the target histogram from the file
//     TH1F* th1_tmp = dynamic_cast<TH1F*>(dFile->Get(dHistName.c_str()));

//     double binWidth = th1_tmp->GetXaxis()->GetBinWidth(1);

//     // refill data hist into cropped hist
//     // Find bin numbers corresponding to xMin and xMax
//     int binMin = th1_tmp->GetXaxis()->FindBin(mass_min);
//     int binMax = th1_tmp->GetXaxis()->FindBin(mass_max);

//     Int_t nbinsDat=  (mass_max - mass_min)/binWidth;
//     // Create a new histogram with a smaller x-range
//     TH1F* th1 = new TH1F("th1", "Cropped data hist", nbinsDat, mass_min, mass_max);

//     // Loop over bins in the specified range
//     for (int bin = binMin; bin <= binMax; ++bin) {
//         // Get the x value for the current bin
//         double xValue = th1_tmp->GetXaxis()->GetBinCenter(bin);

//         // Set bin content and error in the new histogram
//         th1->SetBinContent(bin - binMin + 1, th1_tmp->GetBinContent(bin));
//         th1->SetBinError(bin - binMin + 1, th1_tmp->GetBinError(bin));
//     }


//     th1->Rebin(rebin_factor);

    
//     RooDataHist* dHist = new RooDataHist("dataHist", "RooDataHist from TH1", RooArgSet(*mass), th1);
//     Double_t dInt = dHist->sumEntries();
//     // Specify the ROOT file containing templates and list of histogram names
//     std::string rootFileName = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/inclusiveDiLeptonMC/full/merged/template_hists/merged_templates.root";


//     std::vector<templatePlotDescription> fitTemplates = {
//         {"isEta2MuMu",              42},
//         {"isEta2MuMuGamma",         8},
//         {"isEtaPrime2MuMuGamma",    45},
//         {"isOmega2MuMu",            46},
//         {"isOmega2Pi0MuMu",         40},
//         {"isRho2MuMu",              38},
//         {"isPhi2MuMu",              6},
//         // {"isPhi2KK",                7},
//         {"isKK2mumu",               5},
//         {"isPiPi2mumu",             8},
//         {"combinatorial",           29},
       
//     };

    

//     TCanvas *c = new TCanvas("rf706_histpdf", "rf706_histpdf", 800, 400);

//     // RooPlot *frame1 = mass.frame();


//     // Create a RooArgList to hold the individual PDFs
//     RooArgList pdfList;

//     // Create RooHistPdfs for the specified histograms
//     // std::vector<RooHistPdf*>   histPDFs =  createRooHistPdfs(rootFileName, fitTemplates, mass, rebin_factor);
//     std::vector<templateHistogram>  template_histograms =  makeTemplateHistograms(rootFileName, fitTemplates, mass, rebin_factor);
//     std::vector<RooExtendPdf*> eHistPDFs;
//     std::vector<RooRealVar*>   histNorms;
    

//     // compute total number of evts in MC templates
//     Double_t n_MC_evts=0;
//     for (const auto& tmplt : template_histograms){
//         n_MC_evts+=tmplt.histData->sumEntries();
//     }

//     // for (const auto& histPdf : histPDFs) {
//     // for (RooHistPdf* histPdf : histPDFs){
//     for (size_t i = 0; i < fitTemplates.size(); ++i){

//         std::cout<< "####### Composing template" <<std::endl;
//         std::cout<< fitTemplates[i].normHistPdfName <<std::endl;

//         RooRealVar* norm = new RooRealVar(fitTemplates[i].normHistPdfName.c_str(), fitTemplates[i].normHistPdfName.c_str(), 0, dInt);

//         // set initial normalisation as in total MC histogram
//         Double_t normalisation = dInt * template_histograms[i].histData->sumEntries() / n_MC_evts;
//         norm->setVal(normalisation);
//         RooExtendPdf* ePdf = new RooExtendPdf(fitTemplates[i].extHistPdfName.c_str(), fitTemplates[i].extHistPdfName.c_str(), *template_histograms[i].histPdf, *norm) ;
//         pdfList.add(*ePdf);
//         eHistPDFs.push_back(ePdf);
//         histNorms.push_back(norm);
        
//     }

//     // Create the sum PDF using RooAddPdf
//     RooAddPdf sumPdf("sumPdf", "Sum of PDFs", pdfList);

//     // Fit the sum PDF to the target histogram
//     RooFitResult* fitResult = sumPdf.fitTo(*dHist, RooFit::Save(true));

//     // Print fit results
//     fitResult->Print();

//     // Plot the result
//     RooPlot* fitFrame = mass->frame();

//     // Create a canvas and draw the frame
//     TCanvas fitCanv("canvas", "Fit Result", 800, 600);
//     TLegend *legend = new TLegend(0.7, 0.7, 0.9, 0.9);


//     // Create the main pad (upper part)
//     TPad* mainPad = new TPad("mainPad", "Main Pad", 0, 0.3, 1, 1);
//     mainPad->SetBottomMargin(0); // Set bottom margin to zero
//     mainPad->Draw();
//     mainPad->cd();

//     dHist->plotOn(fitFrame , RooFit::LineColor(kBlack), RooFit::Components("dataHist"), RooFit::Name("Data"), DataError(RooAbsData::Poisson));
//     sumPdf.plotOn(fitFrame, RooFit::LineColor(kBlue), RooFit::Components("sumPdf"), RooFit::Name("total pdf"));
//     // Construct a histogram with the pulls of the data w.r.t the curve
//     RooHist *hpull = fitFrame->pullHist("Data", "total pdf");


//     // Create TRatioPlot
//     // Get TH1 histogram from RooHistPdf
//     TH1* histFromPdf = sumPdf.createHistogram("pdf hist", *mass, RooFit::Binning(mass->getBins()), RooFit::Extended(true));

//     for (size_t i = 0; i < fitTemplates.size(); ++i){
//         // Plot the components with different colors
//         sumPdf.plotOn(fitFrame, RooFit::LineColor(fitTemplates[i].color), RooFit::Components(fitTemplates[i].extHistPdfName.c_str()), RooFit::Name(fitTemplates[i].plotName.c_str()));
//         // Add a legend
//         legend->AddEntry(fitFrame->findObject(fitTemplates[i].plotName.c_str()), fitTemplates[i].plotName.c_str(), "l");
    

    
//     }
//     legend->AddEntry(fitFrame->findObject("Data"), "Data", "l");
//     legend->AddEntry(fitFrame->findObject("total pdf"), "total pdf", "l");
//     fitFrame->Draw();
//     legend->Draw();
    

//     // Create the residuals pad (lower part)
//     fitCanv.cd(); // Move back to the canvas
//     TPad* residualsPad = new TPad("residualsPad", "Residuals Pad", 0, 0, 1, 0.3);
//     // residualsPad->SetPad(0, 0, 1, 0.3);
//     residualsPad->SetTopMargin(0); // Set top margin to zero
//     residualsPad->Draw();
//     residualsPad->cd();
//     // hpull->Draw();



//     // // Compute pulls by hand
//     Double_t dat_val;
//     Double_t pdf_val;

//     Double_t dat_err;
//     Double_t pdf_err;
//     Double_t resid;
//     Double_t resid_err;
//     Double_t data_pdf_err;
    
//     TH1F *histResid = new TH1F("histResid","histResid", histFromPdf->GetNbinsX(), histFromPdf->GetXaxis()->GetXmin(), histFromPdf->GetXaxis()->GetXmax());
//     for (Int_t i=1; i<=histResid->GetNbinsX(); i++) {
//         if (th1->GetBinContent(i) != 0){
//             dat_val = th1->GetBinContent(i);
//             pdf_val = histFromPdf->GetBinContent(i);
//             dat_err = th1->GetBinError(i);
//             pdf_err = histFromPdf->GetBinError(i);
//             // Double_t resid = (th1->GetBinContent(i) - histFromPdf->GetBinContent(i)) / th1->GetBinContent(i);
//             // Double_t resid_err = (th1->GetBinContent(i) - histFromPdf->GetBinContent(i)) / th1->GetBinContent(i);
//             // data_pdf_err = sqrt(pdf_err*pdf_err + dat_err*dat_err);
//             // resid = (dat_val - pdf_val) / dat_val;
//             // resid_err = sqrt( (pdf_err / dat_val) * (pdf_err / dat_val) + (pdf_val * dat_err / dat_val / dat_val) * (pdf_val * dat_err / dat_val / dat_val) );

//             resid = (dat_val - pdf_val) / pdf_err;
//             histResid->SetBinContent(i,resid);
//             // histResid->SetBinError(i,resid_err);
//         }
        
//     }
//     histResid->Draw();
//     // histResid->GetYaxis()->SetRangeUser(-100, 100);



//     // hpull->GetYaxis()->SetRangeUser(-100, 100);



    
//     // fitCanv.SetLogy();
//     // fitCanv.Draw();
//     fitCanv.SaveAs("FitResult_withHLT.pdf");

//     TCanvas fitCanv2("canvas", "Fit Result", 800, 600);
//     fitFrame->Draw();
//     legend->Draw();
//     fitCanv2.SetLogy();
//     fitCanv2.SaveAs("FitResult_withHLT_log.pdf");

//     // // sumPdf.plotOn(frame1);
//     // frame1->GetYaxis()->SetTitleOffset(1.4);
//     // frame1->Draw();
//     // c->SaveAs("./tmp.pdf");


//     // // Clean up allocated memory
//     // for (const auto& histPdf : histPDFs) {
//     //     delete histPdf;
//     // }
//     return 0;
// }
