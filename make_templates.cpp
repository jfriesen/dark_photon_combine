#include <iostream>
#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <yaml-cpp/yaml.h>

#include "template_helpers.h"
#include "helpers.h"




int make_templates() {

    // Load the YAML configuration file
    YAML::Node config_ana = YAML::LoadFile("config_ana.yaml");

    // the data are processed in chunks. We will load one file, merged_{i}.root, select needed decays and create a corresponding inputFileName{i}.root 
    // those temporary tmp_outputFileName{i}.root are merged into output_template_f_name at the end
    std::string MCFilesPath = config_ana["dataset_parameters"]["processed_incl_MC_path"].as<std::string>();
    std::string inputFileName  = MCFilesPath + config_ana["dataset_parameters"]["processed_incl_MC_name"].as<std::string>();
    std::string tmp_outputDir  = MCFilesPath + "template_hists/"
    ensure_directory_exists(tmp_outputDir);
    std::string tmp_outputFileName  = tmp_outputDir + "hist_templates_";
    // std::string output_template_f_name  = MCFilesPath + config_ana["dataset_parameters"]["template_f_name"].as<std::string>();
    std::string output_template_f_name   = MCFilesPath + config_ana["dataset_parameters"]["template_woHLT_f_name"].as<std::string>();

    // all inclusive MC is merged into n_files files
    int n_files = config_ana["dataset_parameters"]["n_MC_files"].as<int>();
    // to split processed files in chunks if needed
    int i_start = 0;

    
    Double_t mass_min = config_ana["hunt_parameters"]["mass_spectrum_min"].as<double>();
    Double_t mass_max = config_ana["hunt_parameters"]["mass_spectrum_max"].as<double>();
    Double_t bin_width = config_ana["hunt_parameters"]["mass_spectrum_bin_width"].as<double>();
    Int_t n_bins = (mass_max - mass_min)/bin_width;

    std::string hist_var = "mass";

    // with HLT requirement
    // std::string base_selection = "(hltResult[0] == 1 || hltResult[1] == 1 )";
    // wo HLT requirement
    std::string base_selection = "(hltResult[0] == 1 || hltResult[0] == 0 )";

    // list of bin cut for eta, use dummy cuts to have bin defined but without actual cut that reduces stats
    std::vector<std::string> binning1_cuts = {
         "(mass > 0.)", 
         "(mass > 0.1)",
    };

    // Loading pt_bins
    std::vector<double> pt_bins;
    for (const auto& value : config_ana["hunt_parameters"]["pt_bins"]) {
        pt_bins.push_back(value.as<double>());
    }

    // Printing pt_bins
    std::cout << "Using the following pt_bins: ";
    for (const auto& pt : pt_bins) {
        std::cout << pt << " ";
    }
    std::cout << std::endl;
    std::vector<std::string> binning2_cuts;
    for (int idx=0; idx < pt_bins.size() - 1; idx++){
        // std::string cut = "(pt1 >= " + std::to_string(pt_bins[idx]) + " && pt1 < " + std::to_string(pt_bins[idx+1]) + " )";
        std::string cut = "(pt >= " + std::to_string(pt_bins[idx]) + " && pt < " + std::to_string(pt_bins[idx+1]) + " )"; 
        binning2_cuts.push_back(cut);

    }

    // Loading decay cuts to form templates
    std::vector<templateSel> decays_selections;
    for (const auto& value : config_ana["templates_parameters"]["decays_selections"]) {
        templateSel sel;
        sel.name = value["name"].as<std::string>();
        sel.selection = value["condition"].as<std::string>();
        decays_selections.push_back(sel);
    }

    // concatenate selections and get a bin histo for each of the decays
    std::vector<templateSel> selections;
    for (const templateSel& sel_decay : decays_selections){
        int bins_count = 0;
        for (const std::string& cut_1 : binning1_cuts){
            for (const std::string& cut_2 : binning2_cuts){
                templateSel sel;
                sel.name = sel_decay.name + "_" + std::to_string(bins_count);
                sel.selection = sel_decay.selection + " && " +  cut_1 + " && " + cut_2;
                selections.push_back(sel);
                std::cout << "Bin name:   " << sel.name << "   bin selection:   " << sel.selection  <<  std::endl;
                bins_count++;

            }
        }
    }


  

     // List of input ROOT files containing histograms, to merge
    std::vector<std::string> templtHistoFiles;

    for (int i = i_start; i< n_files; ++i){
        std::cout << "processing file # " << i << std::endl;
        std::string in_f = inputFileName + std::to_string(i) + ".root";
        std::string out_f = tmp_outputFileName + std::to_string(i) + ".root";
        templtHistoFiles.push_back(out_f);
        createHistograms(in_f.c_str(), out_f.c_str(), n_bins, mass_min, mass_max, hist_var, selections, base_selection);
    }
    

    // Output ROOT file for the merged histograms
    mergeHistograms(templtHistoFiles, output_template_f_name);

    return 0;
}
