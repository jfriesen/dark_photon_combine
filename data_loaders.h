#include <fstream>
#include <cmath>
// #include <experimental/filesystem>
#include "boost/filesystem.hpp"

using namespace RooFit;
namespace fs = boost::filesystem;


RooDataHist* loadObsData(RooRealVar* mass_obs, Int_t rebin_factor, 
                         std::string file_name = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/histoMMGFullRun3.root",
                         std::string hist_name = "pfCandPhotons_minDr_massDimu_all"){
    // Load data histogram and make RooDataHist, 
    TFile* dFile = TFile::Open(file_name.c_str());
    // Get the target histogram from the file
    TH1F* th1_tmp = dynamic_cast<TH1F*>(dFile->Get(hist_name.c_str()));
    // Check if the histogram is found in the file, skip this tmplt otherwise
    if (!th1_tmp) {
        std::cerr << "Error: Histogram '" << hist_name << "' not found in file:  " +  file_name << std::endl;
    }

    Double_t mass_min = mass_obs->getMin();
    Double_t mass_max = mass_obs->getMax();
    // refill data hist into cropped hist
    // Find bin numbers corresponding to xMin and xMax
    int binMin = th1_tmp->GetXaxis()->FindBin(mass_min);
    int binMax = th1_tmp->GetXaxis()->FindBin(mass_max);
    double binWidth = th1_tmp->GetXaxis()->GetBinWidth(1);
    Int_t nbinsDat=  (mass_max - mass_min)/binWidth;
    // Create a new histogram with a smaller x-range
    TH1F* th1 = new TH1F("th1", "Cropped data hist", nbinsDat, mass_min, mass_max);

    // Loop over bins in the specified range
    for (int bin = binMin; bin <= binMax; ++bin) {
        // Get the x value for the current bin
        double xValue = th1_tmp->GetXaxis()->GetBinCenter(bin);

        // Set bin content and error in the new histogram
        th1->SetBinContent(bin - binMin + 1, th1_tmp->GetBinContent(bin));
        th1->SetBinError(bin - binMin + 1, th1_tmp->GetBinError(bin));
    }

    th1->Rebin(rebin_factor);
    RooDataHist* data_obs = new RooDataHist("data_obs", "", RooArgSet(*mass_obs), th1);

    dFile->Close();

    return data_obs;
    

}
// load a tree and draw mass branch into a histo
TH1F* loadHist(std::string file_name, Double_t mass_min, Double_t mass_max, std::string branch_name, std::string hist_name){
    TFile* dFile = TFile::Open(file_name.c_str());
    // Get the TTree from the input file
    TTree* tree = dynamic_cast<TTree*>(dFile->Get("tree/tree")); 

    double binWidth = 0.001;
    Int_t nbinsDat=  (mass_max - mass_min)/binWidth;
    // Create a new histogram with a smaller x-range
    TH1F* th1 = new TH1F(hist_name.c_str(), "Cropped data hist", nbinsDat, mass_min, mass_max);
    th1->Sumw2();

    // std::string selection = "((muonID1[2] == 1) && (muonID2[2] == 1)) && ( (pt > 11) && (pt < 13) )";
    std::string selection = "";
    std::string draw_expr = branch_name + ">>" + th1->GetName();
    // std::cout<< "Drawing expession=  "<< draw_expr << "  "<< "Selection expression=  "<< selection<< std::endl;
    tree->Draw(draw_expr.c_str(), selection.c_str());

    th1->SetDirectory(0);
    dFile->Close();

    return th1;

}

// Function to recursively search for files matching the name pattern
void searchFiles(const fs::path& dir_path, const std::string& name_pattern, std::vector<std::string>& files) {
    for (const auto& entry : fs::directory_iterator(dir_path)) {
        if (fs::is_regular_file(entry.path()) && entry.path().filename().string().find(name_pattern) != std::string::npos) {
            files.push_back(entry.path().string());
        } else if (fs::is_directory(entry.path())) {
            // Recursively search in subdirectories
            searchFiles(entry.path(), name_pattern, files);
        }
    }
}

std::vector<std::string> getFilesList(std::string data_path, std::string name_pattern){
    std::vector<std::string> files;
    searchFiles(data_path, name_pattern, files);

    return files;
}

TH1F* loadMergedData(std::string files_path){

    TH1F* mergedHist = nullptr;

    // Open the ROOT file containing the workspace with merged histogram
    std::string workspaceFilePath = files_path + "merged_hist.root";
    TFile workspace_file(workspaceFilePath.c_str());
    if (!workspace_file.IsOpen()) {
        std::cout<<"File with merged histogram does not exist"<<std::endl;
        return mergedHist;
    }

    RooWorkspace* workspace = (RooWorkspace*)workspace_file.Get("workspace_storage");
    mergedHist = (TH1F*)workspace->obj("h_0");

    mergedHist->SetDirectory(0);
    workspace_file.Close();
    return mergedHist;
}

void saveMergedData(TH1F* mergedHist, std::string files_path){

    std::string workspaceFilePath = files_path + "merged_hist.root";
    RooWorkspace *workspace = new RooWorkspace("workspace_storage","");
    workspace->import(*mergedHist);

    workspace->writeToFile(workspaceFilePath.c_str());
    std::cout<<"Merged histogram is saved in the workspace  "<< workspaceFilePath << std::endl;
    workspace->Print();
}

TH1F* loadObsHistoFromTrees(
                         int i_start = 0,
                         int i_stop = 74,
                         Double_t mass_min = 0.23,
                         Double_t mass_max = 0.6,
                         Int_t rebin_factor = 1, 
                         std::string files_path = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/run3_23/",
                         std::string file_name_pattern = "mmgTree_",
                         std::string branch_name = "mass"){

    TH1F* mergedHist = nullptr;
    
    std::vector<std::string> files = getFilesList(files_path, file_name_pattern);
    std::cout<<"Merging histograms from file # " <<i_start << " to file # " <<i_stop << " from the total # of " <<files.size() <<std::endl;
    std::vector<TH1F*> histograms;

    if ((i_start < files.size() ) && (i_stop <= files.size()) && (i_start <= i_stop)){
        // Extract the slice using iterators
        std::vector<std::string> slice_files(files.begin() + i_start, files.begin() + i_stop);
        int f_count = 0;
        for (const std::string& f_path : slice_files){
            std::string hist_name = "h_" + std::to_string(f_count);
            TH1F* hh = loadHist(f_path, mass_min, mass_max, branch_name, hist_name);
            histograms.push_back(hh);
            f_count++;

        }
        std::cout<<"loaded # of files == "<< f_count << std::endl;
    }
    else{
        std::cerr << "Invalid slice indices! Returning nullptr" << std::endl;
        return mergedHist;
    }
    
    
    // Iterate over the vector and merge histograms
    for (const auto& hist : histograms) {
        if (mergedHist == nullptr) {
            // Clone the first histogram to initialize the merged histogram
            mergedHist = static_cast<TH1F*>(hist->Clone());
            mergedHist->Sumw2();
        } else {
            // Add subsequent histograms to the merged histogram
            mergedHist->Add(hist);
        }
    }

    mergedHist->Rebin(rebin_factor);
    return mergedHist;

}

// load several histograms from observed data, merge them and return RooDataHist
// it reads already pre-merged histo if not required to recreate!
RooDataHist* loadObsDataFromTree(RooRealVar* mass_obs, Int_t rebin_factor, 
                        //  std::string files_path = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/ParkingDoubleMuonLowMass0/crab_muMuGamma_15Aug2023_ParkingDoubleMuonLowMass0/230918_135712/0000/",
                         std::string files_path = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/run3_23/",
                         std::string file_name_pattern = "mmgTree_",
                         std::string branch_name = "mass",
                         int n_files = 174,
                         bool recreate = false){


    // Create an empty TH1F 
    TH1F* mergedHist = nullptr;
    Double_t mass_min = mass_obs->getMin();
    Double_t mass_max = mass_obs->getMax();

    // use merged histo if exists
    if (!recreate){
        mergedHist = loadMergedData(files_path);
        // was it already created? create one otherwise
        if (mergedHist){
            std::cout<<"Loaded merged histogram"<<std::endl;
        }
        else {
            RooDataHist* data_obs_recreated = loadObsDataFromTree(mass_obs, rebin_factor, files_path, file_name_pattern, branch_name, n_files, true);
            return data_obs_recreated;
        }
    }
    else{
        std::cout<<"Merging histograms"<<std::endl;
        std::vector<std::string> files = getFilesList(files_path, file_name_pattern);
        std::vector<TH1F*> histograms;

        int f_count = 0;
        for (const std::string& f_path : files){
            std::string hist_name = "h_" + std::to_string(f_count);
            TH1F* hh = loadHist(f_path, mass_min, mass_max, branch_name, hist_name);
            histograms.push_back(hh);
            f_count++;
            if (f_count >= (n_files)) break;
        }
        std::cout<<"loaded # of files == "<< f_count << std::endl;
        
        
        // Iterate over the vector and merge histograms
        for (const auto& hist : histograms) {
            if (mergedHist == nullptr) {
                // Clone the first histogram to initialize the merged histogram
                mergedHist = static_cast<TH1F*>(hist->Clone());
                mergedHist->Sumw2();
            } else {
                // Add subsequent histograms to the merged histogram
                mergedHist->Add(hist);
            }
        }

        
        saveMergedData(mergedHist, files_path);
        
        // Cleanup: Delete histograms from memory
        for (auto hist : histograms) {
            delete hist;
        }
    }

    mergedHist->Rebin(rebin_factor);
    RooDataHist* data_obs = new RooDataHist("data_obs", "", RooArgSet(*mass_obs), mergedHist);


    

    return data_obs;
    

}

// load obs data histogram for each fill
std::vector<std::pair<int, TH1*>> loadFillHistograms(
                         Int_t rebin_factor, 
                         std::string files_path = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/run3_23/mass_dimu_fill.root"){

    std::vector<std::pair<int, TH1*>> result;
    double nEvtsMin = 300000;

    // Open the ROOT file
    TFile* inputFilePtr = TFile::Open(files_path.c_str());
    // Loop over histograms in the input file
    TIter nextkey(inputFilePtr->GetListOfKeys());
    TKey* key;
    while ((key = dynamic_cast<TKey*>(nextkey()))) {
        TObject* obj = key->ReadObj();
        if (obj->IsA()->InheritsFrom("TH1")) {
            TH1* inputHist = dynamic_cast<TH1*>(obj);
            std::string hName(inputHist->GetName());
            double nEvts = inputHist->GetEntries();
            std::cout << "Processinng a histogram  "<< hName << "with nEvts= " << nEvts<< std::endl;
            if(nEvts > nEvtsMin){
                size_t pos = hName.find("fill");
                // Extract the substring that starts after "fill"
                std::string numberString = hName.substr(pos + 4); // "fill" has 4 characters
                // Convert the substring to an integer
                int nBin;
                std::istringstream(numberString) >> nBin;
                std::pair<int, TH1*> aRes;
                aRes.first = nBin;
                inputHist->SetDirectory(0);
                aRes.second = inputHist;
                result.push_back(aRes);
            }
            else {
                std::cout << "Skipped!" << std::endl;

            }
            

        }
    }
    inputFilePtr->Close();

    return result;
}

// load histograms for bins of pt x eta
std::vector<std::pair<int, TH1*>> loadBinnedDataHistograms(
                         Int_t rebin_factor, 
                         std::string files_path = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/run3_23/merged_binned_data_histos.root"){

    std::vector<std::pair<int, TH1*>> result;
    double nEvtsMin = 300000;

    // Open the ROOT file
    TFile* inputFilePtr = TFile::Open(files_path.c_str());
    // Loop over histograms in the input file
    TIter nextkey(inputFilePtr->GetListOfKeys());
    TKey* key;
    while ((key = dynamic_cast<TKey*>(nextkey()))) {
        TObject* obj = key->ReadObj();
        if (obj->IsA()->InheritsFrom("TH1")) {
            TH1* inputHist = dynamic_cast<TH1*>(obj);
            std::string hName(inputHist->GetName());
            double nEvts = inputHist->GetEntries();
            std::cout << "Processinng a histogram  "<< hName << "with nEvts= " << nEvts<< std::endl;
            if(nEvts > nEvtsMin){
                std::pair<int, TH1*> aRes;
                aRes.first =  std::stoi(hName);;
                inputHist->SetDirectory(0);
                aRes.second = inputHist;
                result.push_back(aRes);
            }
            else {
                std::cout << "Skipped!" << std::endl;

            }
            

        }
    }
    inputFilePtr->Close();

    return result;
}
