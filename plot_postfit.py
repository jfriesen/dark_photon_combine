import os

import yaml

from pyhelpers import ensure_directory_exists, fixed_params, make_fixed_params_str

from ROOT import *
gSystem.AddIncludePath("-I$CMSSW_BASE/src/ ")
gSystem.Load("$CMSSW_BASE/lib/$SCRAM_ARCH/libHiggsAnalysisCombinedLimit.so")
gSystem.AddIncludePath("-I$ROOFITSYS/include")
gSystem.AddIncludePath("-Iinclude/")


# plot_mass_min, plot_mass_max = 0.35, 0.45


def make_plot(category = "Cat_0_260000_10"):
    plot_mass_min, plot_mass_max = 0.215, 0.6

    # dpCard_parametric_Cat_0_410000_0.txt  
                                                          
    card = f'dpCard_parametric_{category}.txt'
    mass = '0.325'
    year = '2022'
    lumi=0
    if year =='2022':
        lumi = 0.0
    else:
        lumi = 0.0

    # os.system(f"combine -M MultiDimFit -d ../combine_output/{card} --algo none --setParameters r=0.0 --setParameterRanges r=-0.5,1.5 -n SB -m "+mass+" --X-rtd REMOVE_CONSTANT_ZERO_POINT=1 --cminDefaultMinimizerTolerance=0.001 --saveWorkspace")
    # os.system(f"combine -M MultiDimFit -d ../combine_output/{card} --algo none --setParameters r=0.0 --setParameterRanges r=-0.000001,0.000001 -n Bonly -m "+mass+" --X-rtd REMOVE_CONSTANT_ZERO_POINT=1 --cminDefaultMinimizerTolerance=0.001 --saveWorkspace")


    # extra_options = " --freezeParameters cb_sigma_isKK2mumu"

    # Load the YAML configuration
    with open('config_ana.yaml', 'r') as file:
        config_ana = yaml.safe_load(file)
    eta2mumugamma_params    = config_ana['free_shape_parameters']['eta2mumugamma_params']
    omega2mumupi_params     = config_ana['free_shape_parameters']['omega2mumupi_params']
    combinatorial_params    = config_ana['free_shape_parameters']['combinatorial_params']
    KK2mumu_params          = config_ana['free_shape_parameters']['KK2mumu_params']
    eta2mumu_params         = config_ana['free_shape_parameters']['eta2mumu_params']

    # To overload params from config, uncommnet the following list
    #  Comment any param below to make it float  
    # eta2mumugamma_params = [
    #     'c_exp_e2mmg',
    #     'c0_e2mmg',
    #     'c1_e2mmg',
    #     'frac_expBern_e2mmg',
    #     # 'err_m_e2mmg',
    #     # 'err_s_e2mmg',
    #     'err2_m_e2mmg',
    #     'err2_s_e2mmg',
    # ]
    # omega2mumupi_params = [
    #     'c_exp_omega2pi0mm',
    #     'c0_omega2pi0mm',
    #     'c1_omega2pi0mm',
    #     'frac_expBern_omega2pi0mm',
    #     'err_m_omega2pi0mm',
    #     'err_s_omega2pi0mm'
    # ]
    # combinatorial_params = [
    #     'a20',
    #     'a21',
    #     'a22',
    #     'a23'
    # ]
    # KK2mumu_params = [
    #     'cb_m_isKK2mumu', 
    #     'cb_sigma_isKK2mumu',
    #     'cb_alpha_isKK2mumu', 
    #     'cb_n_isKK2mumu', 
    #     'a30', 
    #     'a31', 
    #     'a32', 
    #     'a33', 
    #     'frac_cb_isKK2mumu'
    # ]
    # eta2mumu_params = [
    #     'cb_m_isEta2MuMu', 
    #     'cb_sigma_isEta2MuMu',
    #     'cb_alpha_isEta2MuMu', 
    #     'cb_n_isEta2MuMu', 
    #     'frac_cb_isEta2MuMu'
    # ]
    n_total_shape_params = 32
    n_total_norm_params = 5 # numb of bkg processes
    n_fixed_params = len(eta2mumugamma_params + omega2mumupi_params + combinatorial_params + KK2mumu_params + eta2mumu_params)
    n_free_params = n_total_shape_params + n_total_norm_params - n_fixed_params

    print("#"*10 + "   Freezing params manually!")
    extra_options = ','.join( [ f'{param}_{category}' for param in eta2mumugamma_params ] )
    extra_options = extra_options + ',' + ','.join( [ f'{param}_{category}' for param in omega2mumupi_params ] )
    extra_options = extra_options + ',' + ','.join( [ f'{param}_{category}' for param in combinatorial_params ] )
    extra_options = extra_options + ',' + ','.join( [ f'{param}_{category}' for param in KK2mumu_params ] )
    extra_options = extra_options + ',' + ','.join( [ f'{param}_{category}' for param in eta2mumu_params ] )




    extra_options = " --freezeParameters " + extra_options
    extra_param_ranges = ':a06=0.0,5.0:cb_sigma_isKK2mumu=0.005,1.0'

    datacard_dir = config_ana['combine_parameters']['output_dir'] + "datacards/"
    os.system(f"combine -M MultiDimFit -d {datacard_dir}{card} --algo none --setParameters r=0.0 --setParameterRanges r=-0.5,1.5{extra_param_ranges} -n SB -m "+mass+" --X-rtd REMOVE_CONSTANT_ZERO_POINT=1 --cminDefaultMinimizerTolerance=0.00001 --saveWorkspace --cminDefaultMinimizerStrategy 1 --setCrossingTolerance 1e-5" + extra_options)
    os.system(f"combine -M MultiDimFit -d {datacard_dir}{card} --algo none --setParameters r=0.0 --setParameterRanges r=-0.000001,0.000001{extra_param_ranges} -n Bonly -m "+mass+" --X-rtd REMOVE_CONSTANT_ZERO_POINT=1 --cminDefaultMinimizerTolerance=0.00001 --saveWorkspace --cminDefaultMinimizerStrategy 1 --setCrossingTolerance 1e-5" + extra_options)
    print(f"combine -M MultiDimFit -d {datacard_dir}{card} --algo none --setParameters r=0.0 --setParameterRanges r=-0.5,1.5{extra_param_ranges} -n SB -m "+mass+" --X-rtd REMOVE_CONSTANT_ZERO_POINT=1 --cminDefaultMinimizerTolerance=0.00001 --saveWorkspace --cminDefaultMinimizerStrategy 1 --setCrossingTolerance 1e-5" + extra_options)

    f = TFile("higgsCombineSB.MultiDimFit.mH"+mass+".root","READ")
    fb = TFile("higgsCombineBonly.MultiDimFit.mH"+mass+".root","READ")

    w = f.Get("w")
    w.Print()
    w.loadSnapshot("MultiDimFit")

    wb = fb.Get("w")
    wb.loadSnapshot("MultiDimFit")

    m2mu = w.var("mass_obs")
    m2muvars = RooArgSet(m2mu)
    xmin = m2mu.getMin()
    xmax = m2mu.getMax()
    nbins = m2mu.getBinning().numBins()

    print(xmin,xmax,nbins)

    data = w.data("data_obs")
    data = data.reduce(RooFit.SelectVars(m2muvars))  
    rdh = data.binnedClone()
    testhisto = rdh.createHistogram("testhisto", m2mu, RooFit.Binning(nbins, xmin, xmax));
    print([testhisto.GetBinContent(i) for i in range(0,testhisto.GetNbinsX())])
    print(testhisto.Integral())
    #testhisto2 = rdh.createHistogram("testhisto2", m2mu, RooFit.Binning(nbins, xmin, xmax));
    ymin=testhisto.GetMinimum()
    ymax=testhisto.GetMaximum()


    gStyle.SetOptTitle(0); 
    gStyle.SetOptStat(0);

    gStyle.SetPadTopMargin(0.08);
    gStyle.SetPadBottomMargin(0.39);
    gStyle.SetPadLeftMargin(0.15);
    gStyle.SetPadRightMargin(0.07);

    gStyle.SetNdivisions(508, "X");
    gStyle.SetNdivisions(508, "Y");

    c1 = TCanvas("c1", "c1", 650, 720);
    c1.SetLogx(0);
    c1.SetLogy(0);
    c1.SetBottomMargin(0.29);
    c1.SetFillStyle(4000)

    hframe = TH2F("hframe","hframe",500,xmin,xmax,500,0.95*ymin,1.05*ymax);
    hframe.SetXTitle("m_{#mu#mu} [GeV]");

    hframe.GetXaxis().SetLabelSize(25);
    hframe.GetXaxis().SetLabelFont(43);
    hframe.GetXaxis().SetTitleFont(43);
    hframe.GetXaxis().SetTitleSize(25);
    hframe.GetXaxis().SetTitleOffset(4.6);                                                                                                                                                                                                     
    hframe.GetXaxis().SetLabelOffset(0.215);         
    hframe.GetXaxis().SetNdivisions(505);


    hframe.GetYaxis().SetLabelSize(25);
    hframe.GetYaxis().SetLabelFont(43);
    hframe.GetYaxis().SetTitleFont(43);
    hframe.GetYaxis().SetTitleSize(25);
    hframe.GetYaxis().SetNdivisions(505);
    hframe.GetYaxis().SetTitleOffset(1.4);
    hframe.GetYaxis().SetMaxDigits(2)
    hframe.GetYaxis().SetTitle("Events / GeV")
    #hframe.GetYaxis().SetLabelOffset(0.007);

    hframe.GetXaxis().SetRangeUser(plot_mass_min, plot_mass_max)

    frame = m2mu.frame();
    data.plotOn(frame, RooFit.Binning(nbins, xmin, xmax), RooFit.Name("new_hist"), RooFit.MarkerSize(1));
    totalpdf = w.pdf(f"pdf_bin{category}")
    totalpdf.plotOn(frame, RooFit.LineColor(kOrange-3), RooFit.Name("total_pdf"))


    bkgpdf = wb.pdf(f"pdf_bin{category}")
    bkgpdf.plotOn(frame, RooFit.LineColor(4), RooFit.Name("new_pdf"));

    #bkgpdf1 = w.pdf("shapeBkg_bkgModel_Cat0_250000")
    #bkgpdf1.plotOn(frame, RooFit.LineColor(4), RooFit.Name("new_pdf"));

    frame2=m2mu.frame()
    sigpdf = w.pdf(f"shapeSig_signalModel_{category}")
    #nsig = w.function("n_exp_binCat"+card+"_proc_signalModel").getVal()
    ##sigpdf.plotOn(frame2, RooFit.LineColor(kOrange+1), RooFit.Name("sig_pdf"), RooFit.Normalization(nsig,RooAbsReal.NumEvent))
    #print("number of events under sig: " + str(nsig))


    testpdf   = bkgpdf .createHistogram("testpdf"  , m2mu, RooFit.Binning(nbins, xmin, xmax));
    testpdf.Scale(testhisto.Integral(1,-1)/testpdf.Integral(1,-1));

    sbpdf   = totalpdf .createHistogram("totalpdf"  , m2mu, RooFit.Binning(nbins, xmin, xmax));
    sbpdf.Scale(testhisto.Integral(1,-1)/sbpdf.Integral(1,-1));


    mychi2 = 0.;
    mychi2sb = 0.;
    for i in range(1,testhisto.GetNbinsX()+1):
        if testpdf.GetBinContent(i) != 0:
            tmp_mychi2 = ((testpdf.GetBinContent(i) - testhisto.GetBinContent(i)) * (testpdf.GetBinContent(i) - testhisto.GetBinContent(i))) / testpdf.GetBinContent(i);
            mychi2 += tmp_mychi2
        else:
            print(f"testpdf in bin {i} == 0! This bin is skipped in chi2 calculation ")


        if sbpdf.GetBinContent(i) !=0:
            tmp_mychi2 = ((sbpdf.GetBinContent(i) - testhisto.GetBinContent(i)) * (sbpdf.GetBinContent(i) - testhisto.GetBinContent(i))) / sbpdf.GetBinContent(i);
            mychi2sb += tmp_mychi2;
        else:
            print(f"sbpdf in bin {i} == 0! This bin is skipped in chi2 calculation ")
        # tmp_mychi2 = ((testpdf.GetBinContent(i) - testhisto.GetBinContent(i)) * (testpdf.GetBinContent(i) - testhisto.GetBinContent(i))) / testpdf.GetBinContent(i);
        # tmp_mychi2 = ((sbpdf.GetBinContent(i) - testhisto.GetBinContent(i)) * (sbpdf.GetBinContent(i) - testhisto.GetBinContent(i))) / sbpdf.GetBinContent(i);


    noofparm=n_free_params
    mychi2_final = mychi2/(nbins - (noofparm + 1));
    mychi2_final_sb = mychi2sb/(nbins - (noofparm + 1));
    RooPlot_chi2 = frame.chiSquare("new_pdf", "new_hist", noofparm + 1);
    RooPlot_chi2_sb = frame.chiSquare("total_pdf", "new_hist", noofparm + 2);
    print("doFit:  mychi^2 = "+str(mychi2))
    print("doFit:  mychi^2 s+b = "+str(mychi2sb))
    print("doFit:  mychi^2/(nbins-noofparm-1) = "+str(mychi2_final))
    print("doFit:  RooPlot chi^2/(nbins-noofparm-1) = "+str(RooPlot_chi2))
    print("doFit:  RooPlot s+b chi^2/(nbins-noofparm-1) = "+str(RooPlot_chi2_sb))
    print(f"(nbins - (noofparm + 1))  == ({nbins} - ({noofparm} + 1))")


    sigLegend = TH2F();
    sigLegend.SetLineColor(kOrange-3)
    sigLegend.SetLineWidth(3)

    bkgLegend = TH2F();
    bkgLegend.SetLineColor(4)
    bkgLegend.SetLineWidth(3)

    datLegend = TH2F()
    datLegend.SetMarkerSize(1)
    datLegend.SetMarkerStyle(20)

    leg= TLegend(0.40, 0.75,0.90, 0.87)
    leg.SetBorderSize( 0 )
    leg.SetFillStyle( 1001 )
    leg.SetFillColor(kWhite)
    leg.AddEntry( sigLegend, "Signal + Background Fit",  "L" )
    leg.AddEntry( bkgLegend, "Background Only Fit",  "L" )
    leg.AddEntry( datLegend, "Data ("+year+")",  "P" )



    hframe.Draw("");



    leg.Draw("same")
    frame.Draw("same");
    frame.SetYTitle("Events");

    cmsTag=TLatex(0.24,0.93,"#scale[1.0]{CMS}")
    cmsTag.SetNDC()
    cmsTag.SetTextAlign(11)
    cmsTag.Draw()
    cmsTag2=TLatex(0.35,0.93,"#scale[1.0]{#bf{#it{Preliminary}}}")
    cmsTag2.SetNDC()
    cmsTag2.SetTextAlign(11)
    cmsTag2.Draw()
    cmsTag3=TLatex(0.92,0.93,"#scale[0.68]{#bf{"+str(lumi)+" fb^{-1} (13 TeV)}}")
    cmsTag3.SetNDC()
    cmsTag3.SetTextAlign(31)
    cmsTag3.Draw()


    #frame.SetXTitle("m_{#mu#mu} [GeV]");    


    ratioPad = TPad("BottomPad","",0.,0.08,1.,0.285);
    ratioPad.SetFillStyle(4000);
    ratioPad.SetBottomMargin(0);
    ratioPad.Draw();

    ratioPad.SetFillStyle(4000)
    ratioPad.cd();

    
    for i in range(1,testhisto.GetNbinsX()+1): testhisto.SetBinError(i, TMath.Sqrt(testhisto.GetBinContent(i)))

    Ratio = testhisto.Clone("Ratio");
    RatioSB = testhisto.Clone("RatioSB");
    for i in range(1,testhisto.GetNbinsX()+1):
        Ratio.SetBinContent(i, (testhisto.GetBinContent(i) - testpdf.GetBinContent(i))/TMath.Sqrt(testhisto.GetBinContent(i)));
        Ratio.SetBinError(i, TMath.Sqrt(testhisto.GetBinContent(i)))
        RatioSB.SetBinContent(i, (testhisto.GetBinContent(i) - sbpdf.GetBinContent(i))/TMath.Sqrt(testhisto.GetBinContent(i)));
        RatioSB.SetBinError(i, TMath.Sqrt(testhisto.GetBinContent(i)))

    Ratio.SetFillColor(4);
    Ratio.SetBarWidth(0.8);
    Ratio.SetMarkerColor(1);
    Ratio.SetMarkerSize(1);
    Ratio.SetMarkerStyle(20);
    Ratio.SetLineColor(1);
    Ratio.SetStats(0)

    RatioSB.SetBarWidth(0.8);
    RatioSB.SetMarkerColor(1);
    RatioSB.SetMarkerSize(1);
    RatioSB.SetMarkerStyle(20);
    RatioSB.SetLineColor(kOrange-3);
    RatioSB.SetLineWidth(2);
    RatioSB.IsTransparent();
    RatioSB.SetStats(0)

    #Ratio.SetXTitle("m_{#mu#mu} [GeV]");

    Ratio.GetXaxis().SetLabelSize(0);
    Ratio.GetXaxis().SetLabelFont(43);
    Ratio.GetXaxis().SetLabelOffset(0.035);
    Ratio.GetXaxis().SetTitleFont(43);
    Ratio.GetXaxis().SetTitleSize(25);


    Ratio.GetYaxis().SetLabelOffset(0.012);
    Ratio.GetYaxis().SetLabelSize(25);
    Ratio.GetYaxis().SetLabelFont(43);
    Ratio.GetYaxis().SetTitleFont(43);
    Ratio.GetYaxis().SetTitleSize(25);
    Ratio.GetYaxis().SetNdivisions(505);
    # Ratio.GetYaxis().SetRangeUser(-200, 700);
    Ratio.GetYaxis().SetRangeUser(-6, 6);
    Ratio.GetYaxis().SetTitleOffset(1.3);
    Ratio.GetYaxis().SetTitle("Pull")


    Ratio.GetXaxis().SetRangeUser(plot_mass_min, plot_mass_max)
    RatioSB.GetXaxis().SetRangeUser(plot_mass_min, plot_mass_max)

    Ratio.Draw("hist b");
    RatioSB.Draw("hist same");



    #TLine *line1 = TLine(xmin, 1, xmax, 1);
    #line1.SetLineColor(kRed);
    line1 = TLine(xmin, 0, xmax, 0);
    line1.SetLineColor(kBlack);
    line1.SetLineWidth(1);
    line1.Draw("same");


    c1.SaveAs(config_ana['combine_parameters']['output_dir']+"plots/postfit/fit_mass"+category+".pdf");


def main():
    for i in range(0,2):
        make_plot(f"Cat_0_325000_{i}")


if __name__ == "__main__":
    main()        
