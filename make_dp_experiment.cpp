#include "template_helpers.h"
#include "data_loaders.h"
#include "DPhotonRate.h"
#include <fstream>
#include <cmath>
#include "RooPlot.h"
using namespace RooFit;




std::vector<templateHistogram> makeTemplateHistograms(const std::string& rootFileName, std::vector<templateDescription> fitTemplates, RooRealVar* histVar, Int_t rebin_factor) {
    
    std::vector<templateHistogram> templates;
    // Open the ROOT file
    TFile* file = TFile::Open(rootFileName.c_str());

    // Loop over the list of histogram names
    for (const auto& fTemplate : fitTemplates) {
        // Get the histogram from the ROOT file
        std::string histName = fTemplate.name;
        TH1* hist = dynamic_cast<TH1*>(file->Get(histName.c_str()));
        // Check if the histogram is found in the file, skip this tmplt otherwise
        if (!hist) {
            std::cerr << "Error: Histogram '" << fTemplate.name << "' not found in ROOT file." << std::endl;
            continue;
        }
        hist->Rebin(rebin_factor);
        // make Up and Down stat uncty histograms of combaine
        std::pair<TH1F*, TH1F*> hUpDown =  makeUpDownHistograms(hist, fTemplate);
        TH1F* histUp = hUpDown.first;
        TH1F* histDown = hUpDown.second;


        // Create a RooDataHist from the TH1
        RooDataHist* rooDataHist = new RooDataHist(fTemplate.dataHistName.c_str(), "", *histVar, Import(*hist));
        RooDataHist* rooDataHistUp = new RooDataHist(fTemplate.dataHistUpName.c_str(), "", *histVar, Import(*histUp));
        RooDataHist* rooDataHistDown = new RooDataHist(fTemplate.dataHistDownName.c_str(), "", *histVar, Import(*histDown));

        // Create a RooHistPdf from the RooDataHist
        RooHistPdf* rooHistPdf = new RooHistPdf(fTemplate.pdfName.c_str(), fTemplate.pdfName.c_str(), *histVar, *rooDataHist);
        RooHistPdf* rooHistUpPdf = new RooHistPdf(fTemplate.pdfUpName.c_str(), fTemplate.pdfUpName.c_str(), *histVar, *rooDataHistUp);
        RooHistPdf* rooHistDownPdf = new RooHistPdf(fTemplate.pdfDownName.c_str(), fTemplate.pdfDownName.c_str(), *histVar, *rooDataHistDown);

        // make a kernel estimation of the template
        // Adaptive kernel estimation pdf with increased bandwidth scale factor
            // (promotes smoothness over detail preservation)
        RooDataSet* dataSet = rooHistPdf->generate(*histVar, 1e7);
        // RooKeysPdf* kernelPdf = new  RooKeysPdf(fTemplate.pdfName.c_str(), "", *histVar, *dataSet, RooKeysPdf::NoMirror, 2);
        // less smooth one
        RooKeysPdf* kernelPdf = new  RooKeysPdf(fTemplate.pdfName.c_str(), "", *histVar, *dataSet, RooKeysPdf::NoMirror, 1.5);


        // fill out the return object
        templateHistogram tmplt(fTemplate);
        tmplt.name          = fTemplate.name;
        tmplt.histTH1       = hist;
        tmplt.keysPdf       = kernelPdf;
        tmplt.histData      = rooDataHist;
        tmplt.histPdf       = rooHistPdf;
        tmplt.histUpData    = rooDataHistUp;    
        tmplt.histUpPdf     = rooHistUpPdf;
        tmplt.histDownData  = rooDataHistDown; 
        tmplt.histDownPdf   = rooHistDownPdf;

        tmplt.descript      = fTemplate;


        templates.push_back(tmplt);
        

    }
    // Clean up
    file->Close();

    // return dataHists;
    return templates;
}



// it is actually scaling of the shape by param, and not a stat uncty, todo: rename the method
std::string makeStatUnctyParamString(std::string statSystName, int tmpltIdx, const std::vector<templateHistogram> &allTmplt, std::string unctyType = "shape", int nSignals = 1 ){
    // parse a combaine string containing uncty param and processes tha it affects. For ex:
    //  isPhi2MuMu_ shape                      -            1            -         -        -            - 

    std::string statUnctyString = statSystName + "  " + unctyType;
    for (int i = 0; i < nSignals; ++i){
        statUnctyString+= "  -";
    }

    for (int i = 1; i <= allTmplt.size(); ++i){
        if (i == tmpltIdx){ statUnctyString+= "  1.0";}
        else {              statUnctyString+= "  -";}
        
    }

    return statUnctyString;          
}



//// mixed: using RooKeysPdf for the templates, to smooth out low stat ones, and a parametric shape for the sig_model
void dp_experiment_kernel_templates(Double_t test_mass = 0.4, std::string normChannel = "isEta2MuMuGamma_norm") {

    // Scale epsilon coupling, for precision? pretty plotting?
    Double_t param_scale = 1000000.;

    std::string category = "Cat"+std::to_string(test_mass);
    std::replace(category.begin(), category.end(), '.', '_');

    // take these from templates later!
    Double_t mass_min = 0.215;
    Double_t mass_max = 1.4;

    // Double_t mass_min = 0.2;
    // Double_t mass_max = 1.0;
    
    Int_t rebin_factor = 1;

    // main search observable m(mumu), GeV
    RooRealVar* mass_obs = new RooRealVar("mass_obs", "mass_obs", mass_min, mass_max);

    // Specify the ROOT file containing templates and list of histogram names
    // std::string template_f_name = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/inclusiveDiLeptonMC/full/merged/template_hists/merged_templates.root";
    // std::string file_name = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/histoMMGFullRun3.root";
    // std::string hist_name = "pfCandPhotons_minDr_massDimu_all";
    std::string template_f_name = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/inclusiveDiLeptonMC/full/merged/template_hists/merged_templates_pt11to13_isMediumMuon.root";
    std::string data_file_name = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/histosJan29.root";
    std::string data_hist_name = "massDimu_pt11to13_barrel_isMediumMuon";

    // names of proceses correspond to hist names in the root file, if a histogram is not in .root file or needs to be excluded, it has 
    //  to be removed from this list
    std::vector<templateDescription> fitTemplates = {
        {"isEta2MuMu",              42},
        {"isEta2MuMuGamma",         8},
        {"isEtaPrime2MuMuGamma",    45},
        {"isOmega2MuMu",            46},
        {"isOmega2Pi0MuMu",         40},
        {"isRho2MuMu",              38},
        {"isPhi2MuMu",              6},
        // {"isPhi2KK",                7}, // deprecated
        {"isKK2mumu",               7},
        {"isPiPi2mumu",             8},
        {"combinatorial",           29},
       
    };

    
    

    RooDataHist* data_obs = loadObsData(mass_obs, rebin_factor, data_file_name, data_hist_name);
    
    RooWorkspace *dpworkspace = new RooWorkspace("dpworkspace","");
    dpworkspace->import(*data_obs);

    
    TCanvas *c = new TCanvas("prefit", "Prefit kernel estimator shapes", 800, 800);
    TLegend *legend = new TLegend(0.7, 0.7, 0.9, 0.9);
    RooPlot *prefit_frame = mass_obs->frame(Title("Prefit shapes"));

    // used to set the initial vals of the templates normalisations
    Double_t n_MC_evts = 0;
    // load template histograms and add them to the workspace
    std::vector<templateHistogram>  template_histograms =  makeTemplateHistograms(template_f_name, fitTemplates, mass_obs, rebin_factor);
    for (const auto& tmplt : template_histograms){
        std::cout << "Adding " << tmplt.name << " to the workspace"<< std::endl;
        dpworkspace->import(*tmplt.keysPdf);
       
        n_MC_evts+=tmplt.histData->sumEntries();

        tmplt.keysPdf->plotOn(prefit_frame, RooFit::LineColor(tmplt.descript.color), RooFit::Name(tmplt.descript.plotName.c_str()));
        legend->AddEntry(prefit_frame->findObject(tmplt.descript.plotName.c_str()), tmplt.descript.plotName.c_str(), "l");
    }

    // load signal model and import it to the workspace
    RooAddPdf* sig_model = makeSignalModelExample(mass_obs, test_mass);// temporary (where does this shape come from?)
    dpworkspace->import(*sig_model);
    sig_model->plotOn(prefit_frame);
    std::string workspaceFile = "dp_kernel_workspace_" + category +  ".root";
    std::string workspaceFilePath = "../combine_output/" + workspaceFile;
    dpworkspace->writeToFile(workspaceFilePath.c_str());

    // draw prefit shapes for debugging
    c->cd(1);
    prefit_frame->Draw();
    legend->Draw();
    c->SetLogy();
    c->Print("../combine_output/prefit.pdf");


    // init class for computing DPhoton rate
    std::string dPhBfracFName = "resources/bfrac_dp2mumu.txt";
    std::string dPhBfracSplineName = "bfrac_dark_photon_mu_mu";
    DPhotonRate dPhoton(dPhBfracFName, dPhBfracFName);
    Double_t dPhotonRate = dPhoton.getRate(test_mass, normChannel, param_scale);
    

    // Make a Combine datacard
    std::ofstream dp_card("../combine_output/dpKernelCard_" + category + ".txt");
    dp_card << "imax * number of channels" << std::endl;
    dp_card << "jmax * number of background" << std::endl;
    dp_card << "kmax * number of nuisance parameters" << std::endl;
    dp_card << "kmax * number of nuisance parameters"<< std::endl;
    dp_card << "shapes data_obs	    " + category + "  " + workspaceFile + "  dpworkspace:data_obs" << std::endl;

    std::string bins_header         = "bin     " + category;
    std::string processes_names     = "process 		sig_model";
    std::string processes_indxes    = "process 		0";
    std::string processes_rates     = "rate 		1";


    int proc_idx = 1;
    for (const auto& tmplt : template_histograms){
        // add this bkg shape to the combine card
        dp_card << "shapes  " + tmplt.name + "   " + category + "  " + workspaceFile + " dpworkspace:" + tmplt.name  << std::endl;
        bins_header+="    ";
        bins_header+=category;

        processes_names+="    ";
        processes_names+=tmplt.name;

        processes_indxes+="    ";
        processes_indxes+=std::to_string(proc_idx);

        processes_rates+="   1";

        proc_idx++;
    }
     // write signal model to the datacard
    dp_card << "shapes sig_model	    " + category + " " + workspaceFile + " dpworkspace:sig_model" << std::endl;

    dp_card << "bin		"+ category<< std::endl;
    dp_card << "observation     -1.0"<< std::endl;
    dp_card << bins_header << std::endl;
    dp_card << processes_names << std::endl;
    dp_card << processes_indxes << std::endl;
    dp_card << processes_rates << std::endl;
    // add rate param to the signal process that depends on normalisation of Eta2mumugamma (eta2mumu??)
    std::string sigNormalisation = "sig_model_norm rateParam  " + category + "  sig_model  " + "(@0*" + std::to_string(dPhotonRate) + ")  " + normChannel;
    dp_card << sigNormalisation << std::endl;
    // add rate params to each template
    for (const auto& tmplt : template_histograms){
        Double_t normalisation = data_obs->sumEntries() * tmplt.histData->sumEntries() / n_MC_evts;
        // dp_card << tmplt.name + "_norm  rateParam  " + category + "   " + tmplt.name + "  1.0 "  << std::endl;
        dp_card << tmplt.name + "_norm  rateParam  " + category + "   " + tmplt.name + "   " + to_string(normalisation)  + "  [0,1e5]"<< std::endl;

    }





    dp_card.close();

}


//// mixed: using RooHistPDFs for the templates and a parametric shape for the s0g_model
void dp_experiment_roohistpdf(Double_t test_mass = 0.4, std::string normChannel = "isEta2MuMuGamma_norm") {
    // Scale epsilon coupling, for precision? pretty plotting?
    Double_t param_scale = 1000000.;

    std::string category = "Cat"+std::to_string(test_mass);
    std::replace(category.begin(), category.end(), '.', '_');

    // take these from templates later!
    // Double_t mass_min = 0.2;
    // Double_t mass_max = 1.4;

    Double_t mass_min = 0.2;
    Double_t mass_max = 1.0;
    
    Int_t rebin_factor = 1;

    // main search observable m(mumu), GeV
    RooRealVar* mass_obs = new RooRealVar("mass_obs", "mass_obs", mass_min, mass_max);

    // Specify the ROOT file containing templates and list of histogram names
    // std::string template_f_name = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/inclusiveDiLeptonMC/full/merged/template_hists/merged_templates.root";
    // std::string file_name = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/histoMMGFullRun3.root";
    // std::string hist_name = "pfCandPhotons_minDr_massDimu_all";
    std::string template_f_name = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/inclusiveDiLeptonMC/full/merged/template_hists/merged_templates_pt11to13_isMediumMuon.root";
    std::string data_file_name = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/histosJan29.root";
    std::string data_hist_name = "massDimu_pt11to13_barrel_isMediumMuon";

    // names of proceses correspond to hist names in the root file, if a histogram is not in .root file or needs to be excluded, it has 
    //  to be removed from this list
    std::vector<templateDescription> fitTemplates = {
        {"isEta2MuMu",              42},
        {"isEta2MuMuGamma",         8},
        {"isEtaPrime2MuMuGamma",    45},
        {"isOmega2MuMu",            46},
        {"isOmega2Pi0MuMu",         40},
        {"isRho2MuMu",              38},
        {"isPhi2MuMu",              6},
        // {"isPhi2KK",                7}, // deprecated
        {"isKK2mumu",               7},
        {"isPiPi2mumu",             8},
        {"combinatorial",           29},
       
    };

    
    

    RooDataHist* data_obs = loadObsData(mass_obs, rebin_factor, data_file_name, data_hist_name);
    
    RooWorkspace *dpworkspace = new RooWorkspace("dpworkspace","");
    dpworkspace->import(*data_obs);

    // load template histograms and add them to the workspace
    std::vector<templateHistogram>  template_histograms =  makeTemplateHistograms(template_f_name, fitTemplates, mass_obs, rebin_factor);

    // used to set the initial vals of the templates normalisations
    Double_t n_MC_evts = 0;

    for (const auto& tmplt : template_histograms){
        std::cout << "Adding " << tmplt.name << " to the workspace"<< std::endl;
        dpworkspace->import(*tmplt.histPdf);
        dpworkspace->import(*tmplt.histUpPdf);
        dpworkspace->import(*tmplt.histDownPdf);
        n_MC_evts+=tmplt.histData->sumEntries();
    }

    // load signal model and import it to the workspace
    RooAddPdf* sig_model = makeSignalModelExample(mass_obs, test_mass);// temporary (where does this shape come from?)
    dpworkspace->import(*sig_model);
    std::string workspaceFile = "dp_workspace_" + category +  ".root";
    std::string workspaceFilePath = "../combine_output/" + workspaceFile;
    dpworkspace->writeToFile(workspaceFilePath.c_str());

    // init class for computing DPhoton rate
    std::string dPhBfracFName = "resources/bfrac_dp2mumu.txt";
    std::string dPhBfracSplineName = "bfrac_dark_photon_mu_mu";
    DPhotonRate dPhoton(dPhBfracFName, dPhBfracFName);
    Double_t dPhotonRate = dPhoton.getRate(test_mass, normChannel, param_scale);
    

    // Make a Combine datacard
    std::ofstream dp_card("../combine_output/dpCard_" + category + ".txt");
    dp_card << "imax * number of channels" << std::endl;
    dp_card << "jmax * number of background" << std::endl;
    dp_card << "kmax * number of nuisance parameters" << std::endl;
    dp_card << "kmax * number of nuisance parameters"<< std::endl;
    dp_card << "shapes data_obs	    " + category + "  " + workspaceFile + "  dpworkspace:data_obs" << std::endl;

    std::string bins_header         = "bin     " + category;
    std::string processes_names     = "process 		sig_model";
    std::string processes_indxes    = "process 		0";
    std::string processes_rates     = "rate 		1";
    std::vector<std::string> templt_stat_uncties;


    int proc_idx = 1;
    for (const auto& tmplt : template_histograms){
        // add this bkg shape to the combine card
        dp_card << "shapes  " + tmplt.name + "   " + category + "  " + workspaceFile + " dpworkspace:" + tmplt.name + "   dpworkspace:" + tmplt.descript.pdfUpName + "   dpworkspace:" + tmplt.descript.pdfDownName << std::endl;
        bins_header+="    ";
        bins_header+=category;

        processes_names+="    ";
        processes_names+=tmplt.name;

        processes_indxes+="    ";
        processes_indxes+=std::to_string(proc_idx);

        processes_rates+="   1";

        templt_stat_uncties.push_back( 
            makeStatUnctyParamString(
                tmplt.descript.systName, 
                proc_idx, 
                template_histograms
                )
        );


        proc_idx++;
    }
     // write signal model to the datacard
    dp_card << "shapes sig_model	    " + category + " " + workspaceFile + " dpworkspace:sig_model" << std::endl;

    dp_card << "bin		"+ category<< std::endl;
    dp_card << "observation     -1.0"<< std::endl;
    dp_card << bins_header << std::endl;
    dp_card << processes_names << std::endl;
    dp_card << processes_indxes << std::endl;
    dp_card << processes_rates << std::endl;
    // add rate param to the signal process that depends on normalisation of Eta2mumugamma (eta2mumu??)
    std::string sigNormalisation = "sig_model_norm rateParam  " + category + "  sig_model  " + "(@0*" + std::to_string(dPhotonRate) + ")  " + normChannel;
    dp_card << sigNormalisation << std::endl;
    // add rate params to each template
    for (const auto& tmplt : template_histograms){
        Double_t normalisation = data_obs->sumEntries() * tmplt.histData->sumEntries() / n_MC_evts;
        // dp_card << tmplt.name + "_norm  rateParam  " + category + "   " + tmplt.name + "  1.0 "  << std::endl;
        dp_card << tmplt.name + "_norm  rateParam  " + category + "   " + tmplt.name + "   " + to_string(normalisation)  << std::endl;

    }
    // add stat unc-ty on template shapes
    for (std::string& uncty_str : templt_stat_uncties){
        dp_card << uncty_str << std::endl;
    }




    dp_card.close();

}


//// using TH1 shapes of each template for the bkg processes.  sig_model, which is parametric, sampled into TH1 as well
void dp_experiment_th1(Double_t test_mass = 0.4, std::string normChannel = "isEta2MuMuGamma_norm") {

    // Scale epsilon coupling, for precision
    Double_t param_scale = 1000000.;

    std::string category = "Cat"+std::to_string(test_mass);
    std::replace(category.begin(), category.end(), '.', '_');

    // take these from templates later!
    Double_t mass_min = 0.215;
    Double_t mass_max = 1.4;

    // Double_t mass_min = 0.215;
    // Double_t mass_max = 1.0;
    
    Int_t rebin_factor = 1;

    // main search observable m(mumu), GeV
    RooRealVar* mass_obs = new RooRealVar("mass_obs", "mass_obs", mass_min, mass_max);

    // Specify the ROOT file containing templates and list of histogram names
    // std::string template_f_name = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/inclusiveDiLeptonMC/full/merged/template_hists/merged_templates.root"; //merged_templates_woHLT.root
    std::string template_f_name = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/inclusiveDiLeptonMC/full/merged/template_hists/merged_templates_woHLT.root"; //merged_templates_woHLT.root
    std::string data_file_name = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/histoMMGFullRun3.root";
    std::string data_hist_name = "pfCandPhotons_minDr_massDimu_all";
    // std::string template_f_name = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/inclusiveDiLeptonMC/full/merged/template_hists/merged_templates_pt11to13_isMediumMuon.root";
    // std::string data_file_name = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/histosJan29.root";
    // std::string data_hist_name = "massDimu_pt11to13_barrel_isMediumMuon";

    // names of proceses correspond to hist names in the root file, if a histogram is not in .root file or needs to be excluded, it has 
    //  to be removed from this list
    std::vector<templateDescription> fitTemplates = {
        {"isEta2MuMu",              42},
        {"isEta2MuMuGamma",         8},
        // {"isEtaPrime2MuMuGamma",    45},
        {"isOmega2MuMu",            46},
        {"isOmega2Pi0MuMu",         40},
        {"isRho2MuMu",              38},
        {"isPhi2MuMu",              6},
        // {"isPhi2KK",                7}, // deprecated
        {"isKK2mumu",               7},
        {"isPiPi2mumu",             8},
        {"combinatorial",           29},
       
    };

    // get mass histogram from data
    RooDataHist* data_obs = loadObsData(mass_obs, rebin_factor, data_file_name, data_hist_name);
    
    // define and populate the workspace
    RooWorkspace *dpworkspace = new RooWorkspace("dpworkspace","");
    dpworkspace->import(*data_obs);

    // load template histograms and add them to the workspace
    std::vector<templateHistogram>  template_histograms =  makeTemplateHistograms(template_f_name, fitTemplates, mass_obs, rebin_factor);

    // used to set the initial vals of the templates normalisations
    Double_t n_MC_evts = 0;

    for (const auto& tmplt : template_histograms){
        std::cout << "Adding " << tmplt.name << " to the workspace"<< std::endl;
        dpworkspace->import(*tmplt.histPdf);
        dpworkspace->import(*tmplt.histUpPdf);
        dpworkspace->import(*tmplt.histDownPdf);
        n_MC_evts+=tmplt.histData->sumEntries();
    }

    // load signal model and import it to the workspace
    RooAddPdf* sig_model = makeSignalModelExample(mass_obs, test_mass);// temporary (where does this shape come from?)
    dpworkspace->import(*sig_model);
    std::string workspaceFile = "dp_workspace_" + category +  ".root";
    std::string workspaceFilePath = "../combine_output/" + workspaceFile;
    dpworkspace->writeToFile(workspaceFilePath.c_str());

    // init class for computing DPhoton rate
    std::string dPhBfracFName = "resources/bfrac_dp2mumu.txt";
    std::string dPhBfracSplineName = "bfrac_dark_photon_mu_mu";
    DPhotonRate dPhoton(dPhBfracFName, dPhBfracFName);
    Double_t dPhotonRate = dPhoton.getRate(test_mass, normChannel, param_scale);
    
    // save all histograms to the root file
    std::string histFileName = "histograms_" + category +  ".root";
    std::string histFilePath = "../combine_output/" + histFileName;
    TFile *histFile = new TFile(histFilePath.c_str(), "RECREATE");

    // histogram from sig_pdf is sampled to have stat unc-ty
    // Int_t nEvt = 1e6;
    // RooRealVar* observable = dpworkspace->var("mass_obs");
    // RooDataSet* generatedData = sig_model->generate(*observable, nEvt);
    // TH1F* sig_model_hist = new TH1F("sig_model", "", observable->getBins(), observable->getMin(), observable->getMax()); 
    // sig_model_hist->Sumw2(); 
    // generatedData->fillHistogram(sig_model_hist, RooArgList(*observable));

    TH1 *sig_model_hist = dpworkspace->pdf("sig_model")->createHistogram("mass_obs");
    // TH1 *sig_model_hist = sig_model->createHistogram("sig_model", *mass_obs);

    sig_model_hist->SetName("sig_model");
    histFile->WriteTObject(sig_model_hist);


    for (const auto& tmplt : template_histograms){
        std::cout << "Adding " << tmplt.name << " to the root file with histograms"<< std::endl;
        // TH1 *h_tmpl = dpworkspace->pdf(tmplt.name.c_str())->createHistogram("mass_obs");
        TH1 *h_tmpl = tmplt.histData->createHistogram("mass_obs");
        h_tmpl->SetName(tmplt.name.c_str());
        // h_tmpl->Sumw2(); 
        histFile->WriteTObject(h_tmpl);

        // histFile->WriteTObject(tmplt.histTH1);
    }

    TH1 *data_obs_hist = data_obs->createHistogram("mass_obs");
    data_obs_hist->SetName("data_obs"); 
    histFile->WriteTObject(data_obs_hist);


    // Make a Combine datacard
    std::ofstream dp_card("../combine_output/dpTH1Card_" + category + ".txt");
    dp_card << "imax * number of channels" << std::endl;
    dp_card << "jmax * number of background" << std::endl;
    dp_card << "kmax * number of nuisance parameters" << std::endl;
    dp_card << "kmax * number of nuisance parameters"<< std::endl;
    dp_card << "shapes * *	   " + histFileName + "  $PROCESS" << std::endl;

    std::string bins_header         = "bin     " + category;
    std::string processes_names     = "process 		sig_model";
    std::string processes_indxes    = "process 		0";
    std::string processes_rates     = "rate 		1";
    // if sig_model_hist was sampled and not imported (not normalised)
    // std::string processes_rates     = "rate 		-1";

    int proc_idx = 1;
    for (const auto& tmplt : template_histograms){
        // add this bkg shape to the combine card
        bins_header+="    ";
        bins_header+=category;

        processes_names+="    ";
        processes_names+=tmplt.name;

        processes_indxes+="    ";
        processes_indxes+=std::to_string(proc_idx);

        processes_rates+="   ";
        processes_rates+=std::to_string(tmplt.histData->sumEntries());


        proc_idx++;
    }

    dp_card << "bin		"+ category<< std::endl;
    dp_card << "observation     -1.0"<< std::endl;
    dp_card << bins_header << std::endl;
    dp_card << processes_names << std::endl;
    dp_card << processes_indxes << std::endl;
    dp_card << processes_rates << std::endl;

    // add rate param to the signal process that depends on normalisation of normChannel
    //  which is == num evts in tmplt histo x normChannel
    Double_t normChanTemplateSize = 1;
    // find name of tmplt corresponding to the normChannel and eval normChanTemplateSize
    for (const auto& tmplt : template_histograms){
        if( (tmplt.name + "_norm") == normChannel){
            normChanTemplateSize = tmplt.histData->sumEntries();
        }
    }
    
    std::string sigNormalisation = "sig_model_norm rateParam  " + category + "  sig_model  " + "(" + std::to_string(normChanTemplateSize) +  "*@0*" + std::to_string(dPhotonRate) + ")  " + normChannel;
    dp_card << sigNormalisation << std::endl;
    // add rate params to each template
    for (const auto& tmplt : template_histograms){

        // if rate was set to 1 (for histPDFs only)
        // Double_t normalisation = data_obs->sumEntries() * tmplt.histData->sumEntries() / n_MC_evts;
        // if rate was set to -1 (integral of th1 of the shape)
        Double_t normalisation = data_obs->sumEntries() / n_MC_evts;
        // dp_card << tmplt.name + "_norm  rateParam  " + category + "   " + tmplt.name + "  1.0 "  << std::endl;
        dp_card << tmplt.name + "_norm  rateParam  " + category + "   " + tmplt.name + "   " + std::to_string(normalisation) + "  [0,1e4]" << std::endl;

    }
    // add stat unc-ty on template shapes
    dp_card << "* autoMCStats 3000" << std::endl;
    



    dp_card.close();

}

// wraper to have two types of shapes in the experiment:
//      TH1: using TH1 shapes of each template for the bkg processes.  sig_model, which is parametric, sampled into TH1 as well
//      mixed: using RooHistPDFs for the templates and a parametric shape for the sig_model
void make_dp_experiment(Double_t test_mass = 0.4, std::string normChannel = "isEta2MuMuGamma_norm", std::string shapesType = "TH1"){

    if (shapesType == "mixed"){
        dp_experiment_roohistpdf(test_mass, normChannel);
    }
    else if (shapesType == "TH1"){
        dp_experiment_th1(test_mass, normChannel);
    }
    else if (shapesType == "kernelPDF"){
        dp_experiment_kernel_templates(test_mass, normChannel);
    }
    else {
        std::cout<< shapesType + " is not implemented" << std::endl;
    }

}

void makeAllExperiments(std::string normChannel = "isEta2MuMuGamma_norm", std::string shapesType = "kernelPDF"){
    Double_t mass_start = 0.22;
    Double_t mass_stop = 0.47;
    Double_t mass_step = 0.01;

    Double_t test_mass = mass_start;
    while (test_mass <= mass_stop){
        make_dp_experiment(test_mass, normChannel, shapesType);
        test_mass+=mass_step;
    }
    
}
