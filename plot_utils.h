#include <cmath>
#include "RooPlot.h"


// template <typename T>
// void plot_save_result(T &pdf,  RooDataHist* rdh, RooRealVar* obs, std::string plot_path,  std::string title, bool is_logy = false){
//     // Plot the fit result
//     TCanvas canvas("canvas", "Fit Result", 800, 600);
//     RooPlot* frame = obs->frame();
//     frame->SetTitle(title.c_str());
//     rdh->plotOn(frame);
//     pdf.plotOn(frame);
//     if (is_logy) canvas.SetLogy();
//     frame->Draw();
//     canvas.SaveAs(plot_path.c_str());
// }


template <typename T>
void plot_save_result(
    T &pdf,  
    RooDataHist* rdh, 
    RooRealVar* obs, 
    std::string plot_path,  
    std::string title,  
    std::pair<Double_t, Double_t> plot_range, 
    bool is_logy = false,
    std::string leg_pos = "R"
    ){
    // Plot the fit result

    // double xmin = obs->getMin();
    // double xmax = obs->getMax();
    
    
    Double_t xmin = plot_range.first;
    Double_t xmax = plot_range.second;
    int nbins = rdh->numEntries();

    gStyle->SetOptTitle(0); 
    gStyle->SetOptStat(0);

    gStyle->SetPadTopMargin(0.08);
    gStyle->SetPadBottomMargin(0.39);
    gStyle->SetPadLeftMargin(0.15);
    gStyle->SetPadRightMargin(0.07);

    gStyle->SetNdivisions(508, "X");
    gStyle->SetNdivisions(508, "Y");
    TCanvas c1("c1", "c1", 650, 720);
    c1.SetBottomMargin(0.29);
    c1.SetFillStyle(4000);
    RooPlot* frame = obs->frame();
    frame->SetTitle(title.c_str());
    rdh->plotOn(frame);
    pdf.plotOn(frame);
    if (is_logy) c1.SetLogy();



    TH1* testhisto = rdh->createHistogram((title+ "_testhisto").c_str(), *obs);
    // std::cout<<"Debugging number of  bins"<<std::endl;
    // std::cout<<nbins<<std::endl;
    // std::cout<<testhisto->GetNbinsX()<<std::endl;
    // std::cout<<rdh->numEntries()<<std::endl;

    TH1* testpdfh = pdf.createHistogram((title+ "_testpdf").c_str()  , *obs);
    testpdfh->Scale(testhisto->Integral(1,-1)/testpdfh->Integral(1,-1));
    Double_t ymin=testhisto->GetMinimum();
    Double_t ymax=testhisto->GetMaximum();
    TH2F hframe("hframe","hframe",nbins,xmin,xmax,nbins,0.95*ymin,1.05*ymax);
    hframe.SetXTitle("m_{#mu#mu} [GeV]");

    hframe.GetXaxis()->SetLabelSize(25);
    hframe.GetXaxis()->SetLabelFont(43);
    hframe.GetXaxis()->SetTitleFont(43);
    hframe.GetXaxis()->SetTitleSize(25);
    hframe.GetXaxis()->SetTitleOffset(4.6);                                                                                                                                                                                                     
    hframe.GetXaxis()->SetLabelOffset(0.215);         
    hframe.GetXaxis()->SetNdivisions(505);


    hframe.GetYaxis()->SetLabelSize(25);
    hframe.GetYaxis()->SetLabelFont(43);
    hframe.GetYaxis()->SetTitleFont(43);
    hframe.GetYaxis()->SetTitleSize(25);
    hframe.GetYaxis()->SetNdivisions(505);
    hframe.GetYaxis()->SetTitleOffset(1.4);
    hframe.GetYaxis()->SetMaxDigits(2);
    hframe.GetYaxis()->SetTitle("Events / GeV");
    hframe.GetYaxis()->SetLabelOffset(0.007);

    // hframe.GetXaxis()->SetRangeUser(plot_mass_min, plot_mass_max)
    
    // compute chi2
    Double_t chi2 = 0.;
    TH1* Ratio = (TH1*)testhisto->Clone();
    Ratio->SetName(("Ratio_" + title).c_str());
    for (int i = 1; i <= testhisto->GetNbinsX(); ++i) {
        if (testhisto->GetBinContent(i) != 0){
            Ratio->SetBinContent(i, (testhisto->GetBinContent(i) - testpdfh->GetBinContent(i))/TMath::Sqrt(testhisto->GetBinContent(i)));
            Ratio->SetBinError(i, TMath::Sqrt(testhisto->GetBinContent(i)));
            if (testpdfh->GetBinContent(i) != 0) chi2+= ((testpdfh->GetBinContent(i) - testhisto->GetBinContent(i)) * (testpdfh->GetBinContent(i) - testhisto->GetBinContent(i))) / testpdfh->GetBinContent(i);
        }
        else {
            Ratio->SetBinContent(i, 0);
            Ratio->SetBinError(i, 0);
        }
    }
    RooArgSet* floatPars = pdf.getParameters(*rdh);
    double noofparm = floatPars->getSize();
    chi2 = chi2/(nbins - (noofparm + 1));

    
    Double_t x1 = 0.7, y1 = 0.7, x2 = 0.9, y2 = 0.9;
    if (leg_pos == 'L'){
        x1 = 0.2; y1 = 0.7; x2 = 0.4; y2 = 0.9; 
    }
    // Create a legend
    TLegend* legend = new TLegend(x1, y1, x2, y2); // Specify legend position

    // Add entries to the legend
    
    
    legend->SetHeader(title.c_str(),"C");
    legend->AddEntry(frame->getObject(0), "MC", "lep");
    legend->AddEntry(frame->getObject(1), "Background fit", "l");
    legend->AddEntry((TObject*)0, Form("#chi^{2}/nDof = %.2f", chi2), ""); 
    legend->SetBorderSize( 0 );
    // legend->SetFillStyle( 1001 );
    // legend->SetFillColor(kWhite)

    hframe.Draw("");
    frame->Draw("same");
    legend->Draw();

    // make CMS tags
    TLatex cmsTag(0.24,0.93,"#scale[1.0]{CMS}");
    cmsTag.SetNDC();
    cmsTag.SetTextAlign(11);
    cmsTag.Draw();
    TLatex cmsTag2(0.35,0.93,"#scale[0.8]{#bf{#it{Simulation Preliminary}}}");
    cmsTag2.SetNDC();
    cmsTag2.SetTextAlign(11);
    cmsTag2.Draw();
    TLatex cmsTag3(0.92,0.93,"#scale[0.68]{#bf{13.6 TeV}}");
    cmsTag3.SetNDC();
    cmsTag3.SetTextAlign(31);
    cmsTag3.Draw();

    // Draw a ratio pad for pulls
    TPad ratioPad("BottomPad","",0.,0.08,1.,0.255);
    ratioPad.SetFillStyle(4000);
    ratioPad.SetBottomMargin(0);
    ratioPad.Draw("same");

    ratioPad.SetFillStyle(4000);
    ratioPad.cd();

    Ratio->SetFillColor(4);
    Ratio->SetBarWidth(0.8);
    Ratio->SetMarkerColor(1);
    Ratio->SetMarkerSize(1);
    Ratio->SetMarkerStyle(20);
    Ratio->SetLineColor(1);
    Ratio->SetStats(0);
    Ratio->GetXaxis()->SetLabelSize(0);
    Ratio->GetXaxis()->SetLabelFont(43);
    Ratio->GetXaxis()->SetLabelOffset(0.035);
    Ratio->GetXaxis()->SetTitleFont(43);
    Ratio->GetXaxis()->SetTitleSize(25);
    Ratio->GetYaxis()->SetLabelOffset(0.012);
    Ratio->GetYaxis()->SetLabelSize(25);
    Ratio->GetYaxis()->SetLabelFont(43);
    Ratio->GetYaxis()->SetTitleFont(43);
    Ratio->GetYaxis()->SetTitleSize(25);
    Ratio->GetYaxis()->SetNdivisions(505);
    Ratio->GetYaxis()->SetRangeUser(-6, 6);
    Ratio->GetYaxis()->SetTitleOffset(1.3);
    Ratio->GetYaxis()->SetTitle("Pull");
    // Ratio->GetXaxis()->SetTitle("m_{#mu#mu} [GeV]");
    Ratio->Draw("hist b");

    TLine line1(xmin, 0, xmax, 0);
    line1.SetLineColor(kBlack);
    line1.SetLineWidth(1);
    line1.Draw("same");


    c1.SaveAs(plot_path.c_str());
}
