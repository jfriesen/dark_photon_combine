dataset_parameters:
  ###### MC
  # this is a path to processed inclusive dilepton MC files that have our ntuplerizer run on with truthmatched decays
  #  to form our templates. It has merged_0 ... merged_{n_MC_files}
  processed_incl_MC_path: "/eos/user/s/secholak/CMS/eta2mumug/ntuples/inclusiveDiLeptonMC/full/merged/"
  processed_incl_MC_name: "merged_"
  n_MC_files: 31
  # names of MC template histograms that are created by make_templates.cpp and then used to construct datacards by make_dp_parametric...
  # the one without HLT is default one and used for all templates since it has much more stats
  template_woHLT_f_name:  "merged_templates_wohHLT_binned_mumu_pt.root"
  template_f_name:        "merged_templates_binned_mumu_pt.root"

  ###### Data
  # run22G, ~3/fb
  # binned_data_f_name:    "/eos/user/s/secholak/CMS/eta2mumug/ntuples/22D/merged_binned_mumu_pt_data_histos_0p5MeV_mva0p3_partial.root"
  binned_data_f_name:    "/eos/user/s/secholak/CMS/eta2mumug/ntuples/run3_23/merged_binned_mumu_pt_data_histos_0p5MeV.root"
  # the most recent from Jaren, it's split into pt eta and run bins, 62.4/fb run3
  # "/eos/user/s/secholak/CMS/eta2mumug/ntuples/run3_23/mass_mumu_bins_run_pt_mumu.root"

combine_parameters:
  # location of all datacards, plots and condor jobs dirs 
  output_dir: "/afs/cern.ch/work/s/secholak/cms/workdir/eta2mumug/combine/combine_output/"

  # specify particular dir to save jobs in output_dir/condor_sub/condor_subdir/
  # condor_subdir: "22G-0-1/"
  condor_subdir: "22G-0-5-10-19-20-22-25-39/"
  # where CMSSW with combine is
  cmsenv_dir: "/afs/cern.ch/work/s/secholak/cms/workdir/eta2mumug/combine/CMSSW_11_3_4/src/"


hunt_parameters:
  # mass spectrum boundaries (the full fit window for templates) 
  mass_spectrum_min: 0.2
  mass_spectrum_max: 0.6
  mass_spectrum_bin_width: 0.0005

  # mass fit boundaries. This region can be tighter that the one defined above
  # We keep two defenitions untill the fit strategy is completely fixed
  mass_fit_min: 0.215
  mass_fit_max: 0.6

  # mass hunt boundaries. Used to define the list of mass points we hunt on
  mass_start: 0.26
  mass_stop:  0.47
  mass_step:  0.0025

  # scale epsilon coupling, for precision
  param_scale: 1000000.

  # dimuon pt bins that our dataset is split into
  pt_bins: 
    - 6.04
    - 7.81
    - 8.18
    - 8.50
    - 8.82
    - 9.14
    - 9.47
    - 9.81
    - 10.17
    - 10.56
    - 10.97
    - 11.43
    - 11.95
    - 12.54
    - 13.24
    - 14.10
    - 15.18
    - 16.67
    - 18.91
    - 23.29
    - 320.68
    
  # mu1 and mu2 eta bins that our dataset is split into. Its Barrel-Barrel and EndCap-EndCap
  eta_bins:
    - "(abs(eta1) >= 0 && abs(eta1) < 1.48) && (abs(eta2) >= 0 && abs(eta2) < 1.48)"
    - "(abs(eta1) >= 1.48 && abs(eta1) < 3) && (abs(eta2) >= 1.48 && abs(eta2) < 3)"

templates_parameters:
  # cut strings used to get events with background processes to form templates for fitting
  decays_selections:
    - name: "isEta2MuMu"
      condition: "(isEta2MuMu == 1)"

    - name: "isEta2MuMuGamma"
      condition: "(isEta2MuMuGamma == 1)"

    # - name: "isEtaPrime2MuMuGamma"
    #   condition: "(isEtaPrime2MuMuGamma == 1)"
    # - name: "isOmega2MuMu"
    #   condition: "(abs(motherID1) == 223) && (abs(motherID2) == 223) && (isOmega2Pi0MuMu != 1)"

    - name: "isOmega2Pi0MuMu"
      condition: "(isOmega2Pi0MuMu == 1)"

    - name: "isRho2MuMu"
      condition: "(abs(motherID1) == 113) && (abs(motherID2) == 113)"
    # - name: "isPhi2MuMu"
    #   condition: "(abs(motherID1) == 333) && (abs(motherID2) == 333)"
    # KK are missreco as mumu?
    # - name: "isPhi2KK"
    #   condition: "(isPhi2KK == 1)"

    # KK are decaying in flight into mumu
    - name: "isKK2mumu"
      condition: "(abs(motherID1) == 321) && (abs(motherID2) == 321)"

    # - name: "isPiPi"
    #   condition: "(abs(mu1_id) == 211) && (abs(mu2_id) == 211)"
    # - name: "isMuPi"
    #   condition: "(abs(mu1_id) == 13 && abs(mu2_id) == 211) ||( abs(mu2_id) == 13 && abs(mu1_id) == 211)"
    # - name: "isPiPi2mumu"
    #   condition: "(abs(motherID1) == 211) && (abs(motherID2) == 211)"
    # - name: "isMuPi2mumu"
    #   condition: "(abs(mu1_id) == 13 && abs(mu2_id) == 211) ||( abs(mu2_id) == 13 && abs(mu1_id) == 211)"
    # old combinatorial
    # - name: "combinatorial"
    #   condition: "(motherID1 != motherID2) && !((abs(motherID1) == 321) && (abs(motherID2) == 321))"

    # new combinatorial to include everything else
    - name: "combinatorial"
      condition: "!( isEta2MuMu == 1 ) && !( isEta2MuMuGamma == 1 ) && !( isOmega2Pi0MuMu == 1 ) && !( abs(motherID1) == 321 && abs(motherID2) == 321 )"


# these params are free by default when datacards and workspaces are created. When Combine method is called, these params are passed in a list as
#  --frezeParameters. If you need to let params float, comment them out here
free_shape_parameters:
  eta2mumugamma_params:
    - "c_exp_e2mmg"
    - "c0_e2mmg"
    - "c1_e2mmg"
    - "frac_expBern_e2mmg"
    # - "err_m_e2mmg"
    # - "err_s_e2mmg"
    - "err2_m_e2mmg"
    - "err2_s_e2mmg"

  omega2mumupi_params:
    - "c_exp_omega2pi0mm"
    - "c0_omega2pi0mm"
    - "c1_omega2pi0mm"
    - "frac_expBern_omega2pi0mm"
    - "err_m_omega2pi0mm"
    - "err_s_omega2pi0mm"

  combinatorial_params:
    - "a20"
    - "a21"
    - "a22"
    - "a23"

  KK2mumu_params:
    - "cb_m_isKK2mumu"
    - "cb_sigma_isKK2mumu"
    - "cb_alpha_isKK2mumu"
    - "cb_n_isKK2mumu"
    - "a30"
    - "a31"
    - "a32"
    - "a33"
    - "frac_cb_isKK2mumu"

  eta2mumu_params:
    - "cb_m_isEta2MuMu"
    - "cb_sigma_isEta2MuMu"
    - "cb_alpha_isEta2MuMu"
    - "cb_n_isEta2MuMu"
    - "frac_cb_isEta2MuMu"

  rho2mumu_params:
    - "c_exp_isRho2MuMu"
