import os


def ensure_directory_exists(path):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
            print(f"Successfully created directories: {path}")
        except OSError as e:
            print(f"Failed to create directories: {path}, error: {e}")
    else:
        print(f"Directories already exist: {path}")


# make a string of coma separated params list
def fixed_params(category, *param_lists):

    res_condition = ','.join( [ f'{param}_{category}' for param in param_lists[0] ] )
    for pl in param_lists[1:]:
        res_condition = res_condition + ',' + ','.join( [ f'{param}_{category}' for param in pl ] )

    return res_condition

# construct a freezing params condition to fix the needed shape params 
def make_fixed_params_str(m_label, bins, *param_lists):

    extra_options = ','.join( [ fixed_params(f"Cat_{m_label}_{i_bin}", *param_lists) for i_bin in bins ] )
    extra_options = "--freezeParameters " + extra_options

    return extra_options
