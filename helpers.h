#include <iostream>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

void ensure_directory_exists(const std::string& path) {
    fs::path dir(path);
    if (!fs::exists(dir)) {
        if (fs::create_directories(dir)) {
            std::cout << "Successfully created directories: " << path << std::endl;
        } else {
            std::cerr << "Failed to create directories: " << path << std::endl;
        }
    }
    // else {
    //     std::cout << "Directories already exist: " << path << std::endl;
    //     // continue;
    // }
}
