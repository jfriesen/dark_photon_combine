#include <fstream>
#include <cmath>
#include <yaml-cpp/yaml.h>
#include "RooPlot.h"

#include "template_helpers.h"
#include "data_loaders.h"
#include "DPhotonRate.h"
#include "plot_utils.h"
#include "helpers.h"

using namespace RooFit;


// pdf can be any of RooFit pdf types
template <typename T>
void do_chi2_fit(T &pdf,  RooDataHist* rdh, RooRealVar* obs, std::pair<Double_t, Double_t> fit_range){
    std::unique_ptr<RooAbsReal> chi2{
      pdf.createChi2( *rdh, DataError(RooAbsData::SumW2), Range(fit_range.first, fit_range.second))};
    RooMinimizer minimum(*chi2);
    minimum.migrad();
    minimum.hesse();
 
   // Plot chi^2 fit result on frame as well
   std::unique_ptr<RooFitResult> r_chi2_wgt{minimum.save()};
   r_chi2_wgt->Print();
}

template <typename T>
void do_likelyhood_fit(T &pdf,  RooDataHist* rdh, RooRealVar* obs, std::pair<Double_t, Double_t> fit_range){
    pdf.fitTo(*rdh, Save(true), RooFit::Range(fit_range.first, fit_range.second));

}



void fix_params(RooArgList& params){
    for (int i = 0; i < params.getSize(); ++i) {
        RooAbsArg* arg = params.at(i);    
        // Check if the element is a RooRealVar
        if (arg && arg->IsA() == RooRealVar::Class()) {
            RooRealVar* realVar = dynamic_cast<RooRealVar*>(arg);
            realVar->setConstant(true);
        }
    }
}

RooWorkspace* copyAndRenameWorkspace(RooWorkspace* sourceWorkspace, std::string& newNamePostfix){
    RooWorkspace* newWorkspace = new RooWorkspace(*sourceWorkspace);

    // Iterate over all objects in the destination workspace
    TIterator* iter = newWorkspace->componentIterator();
    RooAbsArg* arg;
    while ((arg = (RooAbsArg*)iter->Next())) {
        
        // check if obj was not renamed yet
        std::string oldName = arg->GetName();
        if (oldName.find(newNamePostfix) != std::string::npos){
            std::cout << "Obj.: " << oldName << "  was already renamed"<< std::endl;
        }
        else if (oldName.find("mass_obs") != std::string::npos){
            std::cout << "Obj.: " << oldName << "  is mass observable, should not be re-named!"<< std::endl;
        }
        else if (oldName.find("MH") != std::string::npos){
            std::cout << "Obj.: " << oldName << "  is MH var, should not be re-named!"<< std::endl;
        }
        else {
            // Rename the object
            std::string newName = oldName + "_" + newNamePostfix;
            arg->SetName(newName.c_str());
            std::cout << "Renamed " << oldName << " to " << newName << std::endl;
        }

        
    } 

    return newWorkspace;
}


// load bkg templates workspace, fit them, if does not exists or needs an update
// this one for the given bin number of pt x eta
RooWorkspace* getBinBkgWorkspace( 
                                std::string woHLT_template_f_name,
                                std::string template_f_name,
                                std::string prefit_plots,
                                std::string datacards_path,
                                Double_t mass_min,
                                Double_t mass_max,
                                RooRealVar* mass_obs,
                                int bin_num,
                                bool re_fit = false){

    RooWorkspace* workspace = nullptr;
    std::string bn_str = std::to_string(bin_num);
    // Open the ROOT file containing the workspace with fitted parametric templates
    std::string workspaceFilePath = datacards_path + "bkg_workspace_" + bn_str + ".root";
    

    //  workspace_file.IsOpen()
    // re-fit templates workspace
    if (re_fit){

        workspace = new RooWorkspace("bkg_workspace","");
        // load histograms, define and fit pdfs to data and import those pdfs to the workspace with fixed (or not) params
        // Open the ROOT file
        // TFile* file = TFile::Open(template_f_name.c_str());
        TFile* file_woHLT = TFile::Open(woHLT_template_f_name.c_str());

        //////////////////////////////////////////////////////////////////////////////////// isEta2mumuGamma
        // TH1* h_eta2MuMuGamma = dynamic_cast<TH1*>(file_woHLT->Get("isEta2MuMuGamma"));
        TH1* h_eta2MuMuGamma = dynamic_cast<TH1*>(file_woHLT->Get(("isEta2MuMuGamma_" + bn_str).c_str()));
        RooDataHist* rdh_isEta2MuMuGamma = new RooDataHist("rdh_isEta2MuMuGamma", "", *mass_obs, Import(*h_eta2MuMuGamma));

        // This is a theoretically-inspired function for this decay spectrum; does not work for high pt bins
        // RooRealVar a01("a01", "a01", 3.2591e-01, 2.2460e+00 / 10, 2.2460e+00 * 10);
        // // RooRealVar a02("a02", "a02", 4.4943e-02, 4.4264e-02 / 3, 4.4264e-02 * 3);
        // RooRealVar a02("a02", "a02", 4.4943e-02, 4.4264e-02 / 10, 4.4264e-02 * 10);
        // // RooRealVar a03("a03", "a03", 1.01, 1.01/ 10, 1.01 * 10);
        // RooRealVar a03("a03", "a03", 5/2, 1.01/ 10, 1.01 * 10);
        // RooRealVar a04("a04", "a04", 8.2300e+00, 7.0/ 10, 7.0 * 10);
        // RooRealVar a05("a05", "a05", 9.6726e-01, 6.6726e-01 / 10, 6.6726e-01 * 10);
        // RooRealVar a06("a06", "a06", 2, 0, 3);

        // std::string isEta2MuMuGamma_f = "(@0 < 0.53)?(1/@0^@6)*(1+@1/(@0^2))*(1-@2/(@0^2))^(0.5)*( 1 - @0^2*@4 )^(@3)*(1-@0^2/@5^2)^(-2):0";
        // RooGenericPdf isEta2MuMuGamma("isEta2MuMuGamma", "isEta2MuMuGamma", isEta2MuMuGamma_f.c_str(), RooArgSet(*mass_obs, a01, a02,  a04,a03, a05, a06));

        RooRealVar c_exp_e2mmg("c_exp_e2mmg", "c_exp_e2mmg", -8.8, -15, -1);
        RooExponential exp_e2mmg("exp_e2mmg", "exp_e2mmg", *mass_obs, c_exp_e2mmg);

        RooRealVar c0_e2mmg("c0_e2mmg", "c0_e2mmg", 1, -10, 10);
        RooRealVar c1_e2mmg("c1_e2mmg", "c1_e2mmg", 0.1, -5, 5);
        RooBernstein bern_e2mmg("bern_e2mmg","B", *mass_obs, RooArgList(c0_e2mmg, c1_e2mmg));
        RooRealVar frac_expBern_e2mmg("frac_expBern_e2mmg", "frac_expBern_e2mmg", 0.8, 0.01, 0.99);
        RooAddPdf expPlusBern_e2mmg("expPlusBern_e2mmg", "expPlusBern_e2mmg", 
                                    RooArgList(exp_e2mmg, bern_e2mmg), 
                                    RooArgList(frac_expBern_e2mmg));

        // Turn-on functions from RooEffProd for describing "efficiency x acceptance" and detector effects at low mass
        // -----------------------------------------------------------------------------------------------------------
        // Use error function to simulate the turn-on slope of the trigger
        RooRealVar err_m_e2mmg("err_m_e2mmg", "err_m_e2mmg", 0.215, 0.15, 0.4); 
        RooRealVar err_s_e2mmg("err_s_e2mmg", "err_s_e2mmg", 0.02, 0.001, 0.5);
        RooRealVar err2_m_e2mmg("err2_m_e2mmg", "Error function mean", 0.5, 0.49, 0.55); 
        RooRealVar err2_s_e2mmg("err2_s_e2mmg", "err2_sigma", 0.02, 0.01, 0.05);
        // (0.5 + TMath::ATan((CMS_hgg_mass - err_mean_cat0)/err_sigma_cat0) / (TMath::Pi()))
        RooFormulaVar eff_e2mmg(
                "eff_e2mmg", 
                "(0.5 + 0.5*TMath::Erf((@0 - @1) / (sqrt(2) * @2))) * (0.5 + 0.5*TMath::Erf((@3 - @0) / (sqrt(2) * @4)))", 
                RooArgSet(*mass_obs, err_m_e2mmg, err_s_e2mmg, err2_m_e2mmg, err2_s_e2mmg));

        RooEffProd isEta2MuMuGamma("isEta2MuMuGamma", "isEta2MuMuGamma", expPlusBern_e2mmg, eff_e2mmg);

        std::string plot_path = prefit_plots + "isEta2MuMuGamma_" + bn_str + ".pdf";
        std::pair<Double_t, Double_t> fit_range_isEta2MuMuGamma(mass_min, mass_max);
        // do_chi2_fit(isEta2MuMuGamma,  rdh_isEta2MuMuGamma, mass_obs, fit_range_isEta2MuMuGamma);
        // do_chi2_fit(isEta2MuMuGamma,  rdh_isEta2MuMuGamma, mass_obs, fit_range_isEta2MuMuGamma);

        do_likelyhood_fit(isEta2MuMuGamma,  rdh_isEta2MuMuGamma, mass_obs, fit_range_isEta2MuMuGamma);
        plot_save_result(isEta2MuMuGamma,  rdh_isEta2MuMuGamma, mass_obs, plot_path, "isEta2MuMuGamma", fit_range_isEta2MuMuGamma, false);

        // // RooArgList isEta2MuMuGamma_params(a01, a02, a03, a04, a05);
        RooArgList isEta2MuMuGamma_params(
            c_exp_e2mmg, 
            c0_e2mmg, 
            c1_e2mmg, 
            frac_expBern_e2mmg, 
            // err_m_e2mmg, 
            // err_s_e2mmg, 
            err2_m_e2mmg, 
            err2_s_e2mmg);
        // fix_params(isEta2MuMuGamma_params);
        workspace->import(isEta2MuMuGamma);
        workspace->import(*rdh_isEta2MuMuGamma);

        //////////////////////////////////////////////////////////////////////////////////// isOmega2Pi0MuMu
        TH1* h_isOmega2Pi0MuMu = dynamic_cast<TH1*>(file_woHLT->Get(("isOmega2Pi0MuMu_" + bn_str).c_str()));
        RooDataHist* rdh_isOmega2Pi0MuMu = new RooDataHist("rdh_isOmega2Pi0MuMu", "", *mass_obs, Import(*h_isOmega2Pi0MuMu));

        // does not work for the high pt bins
        // RooRealVar a11("a11", "a11", 2.2460e+00, 2.2460e+00 / 10, 2.2460e+00 * 10);
        // RooRealVar a12("a12", "a12", 4.4264e-02, 4.4264e-02 / 10, 4.4264e-02 * 10);
        // RooRealVar a13("a13", "a13", 1.69, 1.01/ 10, 1.01 * 10);
        // RooRealVar a14("a14", "a14", 7., 7.0/ 10, 7.0 * 10);
        // RooRealVar a15("a15", "a15", 6.6726e-01, 6.6726e-01 / 10, 6.6726e-01 * 10);

        
        // std::string isOmega2Pi0MuMu_f = "(1/@0^2)*(1+@1/(@0^2))*(1-@2/(@0^2))^(0.5)*( (1+@0^2*@3)^2 - @0^2*@4 )^(3/2)*(1-@0^2/@5^2)^(-2)";
        // RooGenericPdf isOmega2Pi0MuMu("isOmega2Pi0MuMu", "isOmega2Pi0MuMu", isOmega2Pi0MuMu_f.c_str(), RooArgSet(*mass_obs, a11, a12, a13, a14, a15));


        RooRealVar c_exp_omega2pi0mm("c_exp_omega2pi0mm", "c_exp_omega2pi0mm", -8.8, -15, -1);
        RooExponential exp_omega2pi0mm("exp_omega2pi0mm", "exp_omega2pi0mm", *mass_obs, c_exp_omega2pi0mm);

        RooRealVar c0_omega2pi0mm("c0_omega2pi0mm", "c0_omega2pi0mm", 1, -10, 10);
        RooRealVar c1_omega2pi0mm("c1_omega2pi0mm", "c1_omega2pi0mm", 0.1, -5, 5);
        RooBernstein bern_omega2pi0mm("bern_omega2pi0mm","B", *mass_obs, RooArgList(c0_omega2pi0mm, c1_omega2pi0mm));
        RooRealVar frac_expBern_omega2pi0mm("frac_expBern_omega2pi0mm", "frac_expBern_omega2pi0mm", 0.8, 0.01, 0.99);
        RooAddPdf expPlusBern_omega2pi0mm("expPlusBern_omega2pi0mm", "expPlusBern_omega2pi0mm", 
                                    RooArgList(exp_omega2pi0mm, bern_omega2pi0mm), 
                                    RooArgList(frac_expBern_omega2pi0mm));
        // errf mean shifts to the right with bin_num [0..20] then again [20...40]
        // this for some reason is not modeled with fit, therefore shifting manually
        double bin_num_corr = 1;
        if (bin_num > 19) bin_num_corr = 0.5;
        RooRealVar err_m_omega2pi0mm("err_m_omega2pi0mm", "err_m_omega2pi0mm", 0.21 + 0.002*bin_num*bin_num_corr, 0.2, 0.4); 
        RooRealVar err_s_omega2pi0mm("err_s_omega2pi0mm", "err_s_omega2pi0mm", 0.04, 0.01, 0.5);
        // (0.5 + TMath::ATan((CMS_hgg_mass - err_mean_cat0)/err_sigma_cat0) / (TMath::Pi()))
        RooFormulaVar eff_omega2pi0mm(
                "eff_omega2pi0mm", 
                "(0.5 + 0.5*TMath::Erf((@0 - @1) / (sqrt(2) * @2))) ", 
                RooArgSet(*mass_obs, err_m_omega2pi0mm, err_s_omega2pi0mm));

        RooEffProd isOmega2Pi0MuMu("isOmega2Pi0MuMu", "isOmega2Pi0MuMu", expPlusBern_omega2pi0mm, eff_omega2pi0mm);

        plot_path = prefit_plots + "isOmega2Pi0MuMu_" + bn_str + ".pdf";
        std::pair<Double_t, Double_t> fit_range_isOmega2Pi0MuMu(mass_min, mass_max);
        do_likelyhood_fit(isOmega2Pi0MuMu,  rdh_isOmega2Pi0MuMu, mass_obs, fit_range_isOmega2Pi0MuMu);
        // do_chi2_fit(isOmega2Pi0MuMu,  rdh_isOmega2Pi0MuMu, mass_obs, fit_range_isOmega2Pi0MuMu);
        plot_save_result(isOmega2Pi0MuMu,  rdh_isOmega2Pi0MuMu, mass_obs, plot_path, "isOmega2Pi0MuMu", fit_range_isOmega2Pi0MuMu);

        // RooArgList isOmega2Pi0MuMu_params(a11, a12, a13, a14, a15);
        RooArgList isOmega2Pi0MuMu_params(
            c_exp_omega2pi0mm,
            c0_omega2pi0mm,
            c1_omega2pi0mm,
            frac_expBern_omega2pi0mm,
            err_m_omega2pi0mm,
            err_s_omega2pi0mm
            );
        // fix_params(isOmega2Pi0MuMu_params);
        workspace->import(isOmega2Pi0MuMu);
        workspace->import(*rdh_isOmega2Pi0MuMu);


        //////////////////////////////////////////////////////////////////////////////////// combinatorial
        TH1* h_combinatorial = dynamic_cast<TH1*>(file_woHLT->Get(("combinatorial_" + bn_str).c_str()));
        RooDataHist* rdh_combinatorial = new RooDataHist("rdh_combinatorial", "", *mass_obs, Import(*h_combinatorial));


        //  1  a20          2.11999e-01   2.28102e-01   3.93966e-06   8.00765e-02
        //  2  a21          6.77493e-01   2.61890e-05   9.75167e-03   1.18097e+00
        //  3  a22          8.29200e+00   6.93792e-04   1.26675e-02   6.69310e-01
        //  4  a23         -5.46282e+00   1.94337e-03   3.51247e-03  -5.77919e-01


        RooRealVar a20("a20", "a20", 0.2113, 0.05, 0.35);
        // RooRealVar a21("a21", "a21", 2.65962e-01, 0.1, 0.7);
        RooRealVar a21("a21", "a21", 6.77493e-01 , 0.1, 0.7);
        RooRealVar a22("a22", "a22", 8.29200e+00, 1.0, 10.0);
        RooRealVar a23("a23", "a23", -5.46282e+00, -10, 10.0);

        // thresh poly
        std::string combinatorial_f = "(@0-@1)**(@2)*(@3 + @0 * @4)";
        RooGenericPdf combinatorial("combinatorial", "combinatorial", combinatorial_f.c_str(), RooArgSet(*mass_obs, a20, a21, a22, a23));

        plot_path = prefit_plots + "combinatorial_" + bn_str + ".pdf";
        std::pair<Double_t, Double_t> fit_range_combinatorial(mass_min, mass_max);
        // do_chi2_fit(combinatorial,  rdh_combinatorial, mass_obs, fit_range_combinatorial);
        do_likelyhood_fit(combinatorial,  rdh_combinatorial, mass_obs, fit_range_combinatorial);

        plot_save_result(combinatorial,  rdh_combinatorial, mass_obs, plot_path, "combinatorial", fit_range_combinatorial);

        // RooArgList combinatorial_params(a20, a21, a22, a23);
        RooArgList combinatorial_params(a20, a22, a23);
        // fix_params(combinatorial_params);
        workspace->import(combinatorial);
        workspace->import(*rdh_combinatorial);



        //////////////////////////////////////////////////////////////////////////////////// isKK2mumu
        TH1* h_isKK2mumu = dynamic_cast<TH1*>(file_woHLT->Get(("isKK2mumu_" + bn_str).c_str()));
        RooDataHist* rdh_isKK2mumu = new RooDataHist("rdh_isKK2mumu", "", *mass_obs, Import(*h_isKK2mumu));

        // KK2mumu combinatorial part + phi2KK(mumu)
        RooRealVar a30("a30", "a30", 0.2113);
        RooRealVar a31("a31", "a31", 3.9388e-01, 0.1, 5.);
        RooRealVar a32("a32", "a32", 6.6790e+00, 1.0, 10.0);
        RooRealVar a33("a33", "a33", -6.6961e+00, -10, 10.0);

        // thresh poly
        std::string isKK2mumu_bkg_f = "(@0-@1)**(@2)*(@3 + @0 * @4)";
        RooGenericPdf isKK2mumu_bkg("isKK2mumu_bkg", "isKK2mumu_bkg", isKK2mumu_bkg_f.c_str(), RooArgSet(*mass_obs, a30, a31, a32, a33));

        //  Parameters for Crystal Ball distribution
        RooRealVar cb_m_isKK2mumu("cb_m_isKK2mumu", "cb_m_isKK2mumu", 3.2295e-01, 0.32, 0.35);
        RooRealVar cb_sigma_isKK2mumu("cb_sigma_isKK2mumu", "cb_sigma_isKK2mumu", 1.3348e-02, 0.01, 1.0);
        RooRealVar cb_alpha_isKK2mumu("cb_alpha_isKK2mumu", "cb_alpha_isKK2mumu", 4.4562e-01, 0, 10.0);
        RooRealVar cb_n_isKK2mumu("cb_n_isKK2mumu", "cb_n_isKK2mumu", 2.2148e+00, 0.1, 10.0);
        RooRealVar frac_cb_isKK2mumu("frac_cb_isKK2mumu", "frac_cb_isKK2mumu", 3.3937e-01, 0.0001, 1.0);
        RooRealVar frac_cb_isKK2mumu_uncty("frac_cb_isKK2mumu_uncty", "frac_cb_isKK2mumu_uncty", 1);
        RooFormulaVar frac_cb_isKK2mumu_constrained("frac_cb_isKK2mumu_constrained", "", "@0*@1", RooArgList(frac_cb_isKK2mumu, frac_cb_isKK2mumu_uncty));

        RooCBShape isKK2mumu_cb("isKK2mumu_cb", "isKK2mumu_cb", *mass_obs, cb_m_isKK2mumu, cb_sigma_isKK2mumu, cb_alpha_isKK2mumu, cb_n_isKK2mumu);

        // RooAddPdf isKK2mumu("isKK2mumu", "isKK2mumu", RooArgList(isKK2mumu_cb, isKK2mumu_bkg), RooArgList(frac_cb_isKK2mumu));
        RooAddPdf isKK2mumu("isKK2mumu", "isKK2mumu", RooArgList(isKK2mumu_cb, isKK2mumu_bkg), RooArgList(frac_cb_isKK2mumu_constrained));

        plot_path = prefit_plots + "isKK2mumu_" + bn_str + ".pdf";
        std::pair<Double_t, Double_t> fit_range_isKK2mumu(mass_min, mass_max);
        // do_chi2_fit(isKK2mumu,  rdh_isKK2mumu, mass_obs, fit_range_isKK2mumu);
        do_likelyhood_fit(isKK2mumu,  rdh_isKK2mumu, mass_obs, fit_range_isKK2mumu);

        plot_save_result(isKK2mumu,  rdh_isKK2mumu, mass_obs, plot_path, "isKK2mumu", fit_range_isKK2mumu);

        RooArgList isKK2mumu_params(
            cb_m_isKK2mumu, 
            cb_sigma_isKK2mumu,
            cb_alpha_isKK2mumu, 
            cb_n_isKK2mumu, 
            a30, a31, a32, a33, 
            frac_cb_isKK2mumu);
        // fix_params(isKK2mumu_params);
        workspace->import(isKK2mumu);
        workspace->import(*rdh_isKK2mumu);


        //////////////////////////////////////////////////////////////////////////////////// isEta2MuMu
        TH1* h_isEta2MuMu = dynamic_cast<TH1*>(file_woHLT->Get(("isEta2MuMu_" + bn_str).c_str()));
        RooDataHist* rdh_isEta2MuMu = new RooDataHist("rdh_isEta2MuMu", "", *mass_obs, Import(*h_isEta2MuMu));

        //  Parameters for Crystal Ball distribution
        RooRealVar cb_m_isEta2MuMu("cb_m_isEta2MuMu", "cb_m_isEta2MuMu", 5.48204e-01, 0.53, 0.56);
        RooRealVar cb_sigma_isEta2MuMu("cb_sigma_isEta2MuMu", "cb_sigma_isEta2MuMu", 5.20324e-03, 0.001, 0.1);
        RooRealVar cb_alpha_isEta2MuMu("cb_alpha_isEta2MuMu", "cb_alpha_isEta2MuMu", 4.4562e-01, 0, 10.0);
        RooRealVar cb_n_isEta2MuMu("cb_n_isEta2MuMu", "cb_n_isEta2MuMu",  1.00024e-01, 0.01, 10.0);
        RooRealVar frac_cb_isEta2MuMu("frac_cb_isEta2MuMu", "frac_cb_isEta2MuMu", 6.94220e-01, 0.0001, 1.0);

        RooCBShape isEta2MuMu_cb("isEta2MuMu_cb", "isEta2MuMu_cb", *mass_obs, cb_m_isEta2MuMu, cb_sigma_isEta2MuMu, cb_alpha_isEta2MuMu, cb_n_isEta2MuMu);
        // Parameters for Gausian
        RooRealVar gs_sigma_isEta2MuMu("gs_sigma_isEta2MuMu", "gs_sigma_isEta2MuMu", 1.18375e-02, 0.001, 0.1);
        RooGaussian isEta2MuMu_gs("isEta2MuMu_gs", "isEta2MuMu_gs", *mass_obs, cb_m_isEta2MuMu, gs_sigma_isEta2MuMu);

        RooAddPdf isEta2MuMu("isEta2MuMu", "isEta2MuMu", RooArgList(isEta2MuMu_cb, isEta2MuMu_gs), RooArgList(frac_cb_isEta2MuMu));

        // RooAddPdf isEta2MuMu_full("isEta2MuMu_full", "isEta2MuMu_full", RooArgList(isEta2MuMu_cb, isEta2MuMu_gs), RooArgList(frac_cb_isEta2MuMu));
        // RooGenericPdf isEta2MuMu("isEta2MuMu", "@0>0.5?@1:1e-15", RooArgSet(*mass_obs, isEta2MuMu_full));
        plot_path = prefit_plots + "isEta2MuMu_" + bn_str + ".pdf";
        // std::pair<Double_t, Double_t> fit_range_isEta2MuMu(0.5, 0.6);
        std::pair<Double_t, Double_t> fit_range_isEta2MuMu(mass_min, mass_max);

        // do_chi2_fit(isEta2MuMu,  rdh_isEta2MuMu, mass_obs, fit_range_isEta2MuMu);
        do_likelyhood_fit(isEta2MuMu,  rdh_isEta2MuMu, mass_obs, fit_range_isEta2MuMu);
        std::pair<Double_t, Double_t> plot_range_isEta2MuMu(0.4, 0.6);
        plot_save_result(isEta2MuMu,  rdh_isEta2MuMu, mass_obs, plot_path,  "isEta2MuMu", plot_range_isEta2MuMu, false, "L");

        // fix all params
        // RooArgList isEta2MuMu_params(cb_m_isEta2MuMu, cb_sigma_isEta2MuMu, cb_alpha_isEta2MuMu, cb_n_isEta2MuMu, gs_sigma_isEta2MuMu, frac_cb_isEta2MuMu);
        // fix all except mass resolution
        RooArgList isEta2MuMu_params(
            cb_m_isEta2MuMu, 
            cb_alpha_isEta2MuMu, 
            cb_n_isEta2MuMu, 
            frac_cb_isEta2MuMu
            );
        // fix_params(isEta2MuMu_params);
        workspace->import(isEta2MuMu);
        workspace->import(*rdh_isEta2MuMu);


        // /////////
        workspace->writeToFile(workspaceFilePath.c_str());
        std::cout<<"Bkg templates are saved in the workspace  "<< workspaceFilePath << std::endl;
        workspace->Print();

        // file->Close();
        file_woHLT->Close();
        
    }
    else{
        TFile workspace_file(workspaceFilePath.c_str());
        if (workspace_file.IsOpen()) {
            std::cout<<"Loaded workspace with fitted templates  "<< workspaceFilePath << std::endl;
            workspace = (RooWorkspace*)workspace_file.Get("bkg_workspace");
        }
        else{
            std::cout<<"The workspace with fitted templates does not exist, recreating  "<< std::endl;
            workspace = getBinBkgWorkspace(woHLT_template_f_name, template_f_name, prefit_plots, datacards_path, mass_min, mass_max, mass_obs, bin_num, true);
        }
            
    }
    
    // // each bin has to have a unique name
    // RooWorkspace* renamedWorkspace =  copyAndRenameWorkspace(workspace, category);
    return workspace;
}




void make_bin_experiment(
                        TH1* bin_hist,
                        int bin_num, 
                        Double_t mass_min, 
                        Double_t mass_max, 
                        Double_t test_mass, 
                        Int_t rebin_factor,
                        YAML::Node config_ana
                        ){


    // files with template histograms
    std::string template_f_name         = config_ana["dataset_parameters"]["processed_incl_MC_path"].as<std::string>() + config_ana["dataset_parameters"]["template_f_name"].as<std::string>();
    std::string template_woHLT_f_name   = config_ana["dataset_parameters"]["processed_incl_MC_path"].as<std::string>() + config_ana["dataset_parameters"]["template_woHLT_f_name"].as<std::string>();

    // search category for given mass and bin number
    std::string category = "Cat_"+std::to_string(test_mass) + "_" + std::to_string(bin_num);
    std::replace(category.begin(), category.end(), '.', '_');

    std::string normChannel = "isEta2MuMu_" + category + "_norm";
    std::string workspaceFile = "dp_workspace_parametric_" + category +  ".root";

    // configure directories for postfit plots and datacards
    std::string combine_output_dir = config_ana["combine_parameters"]["output_dir"].as<std::string>();
    std::string prefit_plots = combine_output_dir + "plots/prefit/";
    ensure_directory_exists(prefit_plots);
    std::string datacards_path = combine_output_dir + "datacards/";
    ensure_directory_exists(datacards_path);

    // Scale epsilon coupling, for precision
    double param_scale = config_ana["hunt_parameters"]["param_scale"].as<double>();

    // main search observable m(mumu), GeV

    RooRealVar* mass_obs = new RooRealVar("mass_obs", "", mass_min, mass_max);
    mass_obs->setBins(bin_hist->GetNbinsX());
    RooDataHist* data_obs = new RooDataHist( ("data_obs_"+category).c_str(), "", RooArgSet(*mass_obs), bin_hist);
    
    // load bkg templates, set True to re-fit/define instead of loading already existing ones
    //  When re-fitting bkg, or fitting a new template for a first time it should be `true` but switched back to `false`
    //  after the first run to save time when defining multiple categories
    RooWorkspace* bkg_workspace = getBinBkgWorkspace(template_woHLT_f_name, template_f_name, prefit_plots, datacards_path, mass_min, mass_max, mass_obs, bin_num, false);

    RooWorkspace *dpworkspace = new RooWorkspace(*bkg_workspace);
    dpworkspace->SetName("dpworkspace");
    dpworkspace->import(*data_obs);

    // load signal model and import it to the workspace
    RooAddPdf* sig_model = makeSignalModelExample(mass_obs, test_mass);// temporary (where does this shape come from?)
    dpworkspace->import(*sig_model);
    
    

    // each bin has to have a unique name
    RooWorkspace* renamedWorkspace =  copyAndRenameWorkspace(dpworkspace, category);
    
    std::string workspaceFilePath = datacards_path + workspaceFile;
    renamedWorkspace->writeToFile(workspaceFilePath.c_str());
    renamedWorkspace->Print();

    // init class for computing DPhoton rate
    std::string dPhBfracFName = "resources/bfrac_dp2mumu.txt";
    std::string dPhBfracSplineName = "bfrac_dark_photon_mu_mu";
    DPhotonRate dPhoton(dPhBfracFName, dPhBfracFName);
    Double_t dPhotonRate = dPhoton.getRate(test_mass, normChannel, param_scale);


    // Make a Combine datacard
    std::ofstream dp_card(datacards_path + "dpCard_parametric_" + category + ".txt");
    dp_card << "imax * number of channels" << std::endl;
    dp_card << "jmax * number of background" << std::endl;
    dp_card << "kmax * number of nuisance parameters" << std::endl;
    dp_card << "kmax * number of nuisance parameters"<< std::endl;
    dp_card << "shapes   data_obs	    "   + category + "  " + workspaceFile + " dpworkspace:data_obs"         + "_" + category << std::endl;
    dp_card << "shapes  isEta2MuMu   "      + category + "  " + workspaceFile + " dpworkspace:isEta2MuMu"       + "_" + category << std::endl;
    dp_card << "shapes  isEta2MuMuGamma   " + category + "  " + workspaceFile + " dpworkspace:isEta2MuMuGamma"  + "_" + category << std::endl;
    dp_card << "shapes  isOmega2Pi0MuMu   " + category + "  " + workspaceFile + " dpworkspace:isOmega2Pi0MuMu"  + "_" + category << std::endl;
    dp_card << "shapes  isKK2mumu   "       + category + "  " + workspaceFile + " dpworkspace:isKK2mumu"        + "_" + category << std::endl;
    dp_card << "shapes  combinatorial   "   + category + "  " + workspaceFile + " dpworkspace:combinatorial"    + "_" + category << std::endl;
    dp_card << "shapes  sig_model	    "   + category + "  " + workspaceFile + " dpworkspace:sig_model"        + "_" + category << std::endl;
    dp_card << "bin		"+ category<< std::endl;
    dp_card << "observation     -1.0"<< std::endl;
    dp_card << "bin     " + category + "   " + category + "   "+ category + "   "+ category + "   "+ category + "   "+ category + "   " << std::endl;
    dp_card << "process 		sig_model   isEta2MuMu   isEta2MuMuGamma   isOmega2Pi0MuMu   isKK2mumu   combinatorial" << std::endl;
    dp_card << "process 		0           1            2                 3                 4           5" << std::endl;
    dp_card << "rate    		1           1            1                 1                 1           1" << std::endl;
    std::string sigNormalisation = "sig_model_"+category+"_norm rateParam  " + category + "  sig_model  " + "(@0*" + std::to_string(dPhotonRate) + ")  " + normChannel;
    dp_card << sigNormalisation << std::endl;


    Double_t n_MC_evts = bkg_workspace->data("rdh_isEta2MuMu")->sumEntries() + bkg_workspace->data("rdh_isEta2MuMuGamma")->sumEntries() + bkg_workspace->data("rdh_isOmega2Pi0MuMu")->sumEntries() + bkg_workspace->data("rdh_isKK2mumu")->sumEntries() + bkg_workspace->data("rdh_combinatorial")->sumEntries();
    dp_card << "isEta2MuMu_"+category+"_norm  rateParam  " + category + "  isEta2MuMu   " + to_string(data_obs->sumEntries() * bkg_workspace->data("rdh_isEta2MuMu")->sumEntries() / n_MC_evts)  << std::endl;
    dp_card << "isEta2MuMuGamma_"+category+"_norm  rateParam  " + category + "  isEta2MuMuGamma   " + to_string(data_obs->sumEntries() * bkg_workspace->data("rdh_isEta2MuMuGamma")->sumEntries() / n_MC_evts)  << std::endl;
    dp_card << "isOmega2Pi0MuMu_"+category+"_norm  rateParam  " + category + "  isOmega2Pi0MuMu   " + to_string(data_obs->sumEntries() * bkg_workspace->data("rdh_isOmega2Pi0MuMu")->sumEntries() / n_MC_evts)  << std::endl;
    dp_card << "isKK2mumu_"+category+"_norm  rateParam  " + category + "  isKK2mumu   " + to_string(data_obs->sumEntries() * bkg_workspace->data("rdh_isKK2mumu")->sumEntries() / n_MC_evts)  << std::endl;
    dp_card << "combinatorial_"+category+"_norm  rateParam  " + category + "  combinatorial   " + to_string(data_obs->sumEntries() * bkg_workspace->data("rdh_combinatorial")->sumEntries() / n_MC_evts)  << std::endl;

    // systematics
    dp_card << "frac_cb_isKK2mumu_uncty_"+category + "  param 1 0.25" << std::endl;


    dp_card.close();
    
    std::cout << "Number of events in RooDataHist: " << data_obs->sumEntries() << std::endl;
}




//  loading binned dataset histograms pt x eta
void make_parametric_dp_experiment(){
    gROOT->SetBatch(kTRUE);

    // Load the YAML configuration file
    YAML::Node config_ana = YAML::LoadFile("config_ana.yaml");

    // boundaries for the scan region
    double mass_start = config_ana["hunt_parameters"]["mass_start"].as<double>();
    double mass_stop  = config_ana["hunt_parameters"]["mass_stop"].as<double>();
    double mass_step  = config_ana["hunt_parameters"]["mass_step"].as<double>();

    //  boundaries for the mass spectrum
    double mass_min = config_ana["hunt_parameters"]["mass_fit_min"].as<double>();
    double mass_max = config_ana["hunt_parameters"]["mass_fit_max"].as<double>();

    // file with binned histograms from the data
    std::string binned_data_f_name      = config_ana["dataset_parameters"]["binned_data_f_name"].as<std::string>(); 

    Int_t rebin_factor = 1;
    std::vector<std::pair<int, TH1*>> hist_collection = loadBinnedDataHistograms(rebin_factor, binned_data_f_name);

    std::vector<int> binNums;

    // loop over data slices and loop internally for masses --> avoiding repeatedly loading data from trees
    for (const auto& pair : hist_collection) {
        int bin_num = pair.first;
        binNums.push_back(bin_num);
        TH1* slice_hist = pair.second;
        Double_t test_mass = mass_start;
        while (test_mass <= mass_stop){
            make_bin_experiment(slice_hist, bin_num, mass_min, mass_max, test_mass, rebin_factor, config_ana);
            test_mass+=mass_step;
            // break;
        }
        // break;
        
    }

}




