import math
import glob
import os
import numpy as np
import yaml

from pyhelpers import ensure_directory_exists, fixed_params, make_fixed_params_str


# Load the YAML configuration
with open('config_ana.yaml', 'r') as file:
    config_ana = yaml.safe_load(file)

# type of combine job to submit
# job_type = " "
# job_type = "FitDiagnostics --saveShapes"
job_type = "MultiDimFit --saveWorkspace"

# cms env with Combine installed
base_path = config_ana['combine_parameters']['cmsenv_dir']
# location of the jobs output
exe_path = config_ana['combine_parameters']['output_dir'] + "condor_sub/"
condor_subdir = config_ana['combine_parameters']['condor_subdir'] 
ensure_directory_exists(exe_path + condor_subdir)

# location of merged datacards
cards_path = config_ana['combine_parameters']['output_dir'] + "datacards/"

# define a range of bumphunt
mass_start  = config_ana['hunt_parameters']['mass_start']
mass_stop   = config_ana['hunt_parameters']['mass_stop']
mass_step   = config_ana['hunt_parameters']['mass_step']

# number of pt x eta bins (20pt, 2eta)
pt_bins = config_ana['hunt_parameters']['pt_bins']
# nbins = n_bin_voundaries - 1
bins = range(0,len(pt_bins) - 1)

# we re-define them for a local test
# bins = [0,1,2,3,4,5, 20,21,22,23,24,25]
bins = [0,1]
m_vals = np.arange(mass_start, mass_stop, mass_step)
m_vals = np.sort(m_vals)
print("##"*45)
print(f"Submitting {len(m_vals)} mass points with {len(bins)} bins each")

# Comment any param below to make it float
# these shape params are free in the combine workspaces. all uncommented params are passed as fixed ones to the combine call  
eta2mumugamma_params    = config_ana['free_shape_parameters']['eta2mumugamma_params']
omega2mumupi_params     = config_ana['free_shape_parameters']['omega2mumupi_params']
combinatorial_params    = config_ana['free_shape_parameters']['combinatorial_params']
KK2mumu_params          = config_ana['free_shape_parameters']['KK2mumu_params']
eta2mumu_params         = config_ana['free_shape_parameters']['eta2mumu_params']
print("#"*10 + "   Freezing params manually!")





# executable script template
executable_template = """#!/bin/bash
source /cvmfs/cms.cern.ch/cmsset_default.sh
cd {base_path}
eval `scramv1 runtime -sh`

cd {mass_path}
combine -M {job_type} {combined_card_name} -m {mass} --rMin -7 --rMax 7 --cminDefaultMinimizerStrategy 1 --cminDefaultMinimizerTolerance 1 {fixed_params_str}
"""


submission_template = """universe = vanilla
executable = {executable_path}
output = {mass_path}/hunt.$(ClusterId).$(ProcId).out
error = {mass_path}/hunt.$(ClusterId).$(ProcId).err
log = {mass_path}/hunt.$(ClusterId).log
should_transfer_files = YES
when_to_transfer_output = ON_EXIT
requirements = (OpSysAndVer =?= "CentOS7")
transfer_input_files = {executable_path}
+JobFlavour = "longlunch"
queue 1
"""
# transfer_input_files = {executable_path},{base_path}/cards/lowmassDipho_fiveCat_RooMultiPdf_cat1_fullStat.txt



# m_vals = [0.467500]
for m in m_vals:
    m = round(m,4)
    m_label = f"{m:.6f}"
    m_label = m_label.replace(".", "_")

    # list of bin-cards for a given mass
    bin_mass_cards = [ f"Cat_{m_label}_{i_bin}={cards_path}dpCard_parametric_Cat_{m_label}_{i_bin}.txt" for i_bin in bins ]
    
    # combine them into a single one 
    combined_cards = "combineCards.py  "
    for exp_bin in bin_mass_cards: combined_cards = combined_cards + exp_bin + " "
    combined_card_name = f"{cards_path}combined_Cat_{m_label}.txt"
    combined_cards += f" > {combined_card_name}"
    print(f"Making a combined datacard for mass={m} with n_bins={len(bins)}")
    print(combined_cards)
    os.system(combined_cards)

    # make a string of fixed params to pass to combine
    fixed_params_str = make_fixed_params_str(m_label, bins, eta2mumugamma_params, omega2mumupi_params, combinatorial_params, KK2mumu_params, eta2mumu_params)

    # os.system("combine -M AsymptoticLimits " + combined_card_name + " -m " + str(m) + " --rMax 3")

    # Generate directories, submission files, and executables
    mass_path = os.path.join(exe_path, condor_subdir, f"m{m_label}")
    os.makedirs(mass_path, exist_ok=True)
    print("base_path = ", base_path)
    print("exe_path = ", exe_path)
    print("mass_path = ", mass_path)

    # Write executable
    executable_path = os.path.join(mass_path, f"hunt_{m}.sh")
    print("executable_path = ", executable_path)
    with open(executable_path, "w") as f:
        f.write(executable_template.format(
            base_path = base_path,
            mass_path = mass_path,
            job_type = job_type,
            combined_card_name = combined_card_name,
            mass = m,
            fixed_params_str = fixed_params_str
            ))

    # Write submission file
    #output_root_file = os.path.join(mass_path, f"higgsCombineTest.GenerateOnly.mH{mass}.123456.root")
    submission_path = os.path.join(mass_path, f"hunt_{m}.submit")
    with open(submission_path, "w") as f:
        f.write(submission_template.format(executable_path=executable_path, mass_path=mass_path))

    os.system("condor_submit " + submission_path)
    print(f"Created directory, executable, and submission file for mass point {m}.")
print("All directories, executables, and submission files created successfully.")
