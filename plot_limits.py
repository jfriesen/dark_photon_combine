import ROOT
import os,sys
from ROOT import TGraphAsymmErrors
from ROOT import TGraphErrors
from ROOT import TColor
#from ROOT import TGraph
from array import array
from ROOT import *
from operator import truediv
import random
import math
from glob import glob
from os.path import join
import re 
import sys
from math import sqrt
import numpy as np

limit1obs=array('d')
limit1=array('d')
limiteps2=array('d')
limit190=array('d')
limiteps290=array('d')
limit195up=array('d')
limit195down=array('d')
limit168up=array('d')
limit168down=array('d')
limit1Observed=array('d')
limit2=array('d')
limit2eps2=array('d')
limit290=array('d')
limit2eps290=array('d')
limit295up=array('d')
limit295down=array('d')
limit268up=array('d')
limit268down=array('d')
limit2Observed=array('d')
mass1=array('d')
mass2=array('d')
masserr1=array('d')
masserr2=array('d')


# lumi = 62.4
# lumi_project = 62.4

lumi = 3.08 #22G
lumi_project = 62.4 # yr22+23
# lumi_project = 170+96.6 # run2+3

param_scale = 1000000.

# subdir = "10bins"
subdir = "22G"

combine_output = f'../combine_output/{subdir}/datacards/'
combine_plots = f'../combine_output/{subdir}/plots/'
if not os.path.exists(combine_plots):
    # If not, create it
    os.makedirs(combine_plots)
    print(f"Directory '{combine_plots}' created.")

# files = glob(combine_output + "higgsCombineTest.AsymptoticLimits.mH*.root")
exe_path = "/afs/cern.ch/work/s/secholak/cms/workdir/eta2mumug/combine/combine_output/condor_sub/"
condor_subdir = "22G/"

files = glob(join(exe_path, condor_subdir, "*/higgsCombineTest.AsymptoticLimits*.root"))

masses = []


for fname in files:
        m = fname.split("mH")[1].rstrip(".root")
        m = float(m)
        if (m<0.26): continue

        masses.append(m)

        f=ROOT.TFile.Open(fname)
        tree=f.Get("limit")

        tree.GetEntry(5)
        limit1obs.append(math.sqrt(math.sqrt(lumi/lumi_project)*tree.limit/param_scale))


        tree.GetEntry(2)
        limit1.append(math.sqrt(math.sqrt(lumi/lumi_project)*tree.limit/param_scale))
        print("m,limit",m,math.sqrt(math.sqrt(lumi/lumi_project)*tree.limit/param_scale))
        
        tree.GetEntry(0)
        limit195up.append(abs(math.sqrt(math.sqrt(lumi/lumi_project)*tree.limit/param_scale)-limit1[-1]))
        
        tree.GetEntry(4)
        limit195down.append(abs(math.sqrt(math.sqrt(lumi/lumi_project)*tree.limit/param_scale)-limit1[-1]))            
        
        tree.GetEntry(1)
        limit168up.append(abs(math.sqrt(math.sqrt(lumi/lumi_project)*tree.limit/param_scale)-limit1[-1]))

        tree.GetEntry(3)
        limit168down.append(abs(math.sqrt(math.sqrt(lumi/lumi_project)*tree.limit/param_scale)-limit1[-1]))
            
        mass1.append(m)
        masserr1.append(0.)

sorted_indices = np.argsort(masses)
masses = np.array(masses)[sorted_indices]
limit1obs       = np.array(limit1obs)[sorted_indices]
limit1          = np.array(limit1)[sorted_indices]
limit195up      = np.array(limit195up)[sorted_indices]
limit195down    = np.array(limit195down)[sorted_indices]
limit168up      = np.array(limit168up)[sorted_indices]
limit168down    = np.array(limit168down)[sorted_indices]
mass1           = np.array(mass1)[sorted_indices]
masserr1        = np.array(masserr1)[sorted_indices]


lhcbmass=array('d')
lhcblimit=array('d')

print(masses)
print(limit1obs)

with open("resources/LHCb_Aaij2019bvg_prompt.lmt","r") as f:
        for line in f:
                print("LHCb",line.split()[0],line.split()[1])

                if (float(line.split()[0])<0.212): continue
                if (float(line.split()[0])>0.5): continue
                
                lhcbmass.append(float(line.split()[0]))
                lhcblimit.append(float(line.split()[1]))

c1=ROOT.TCanvas("c1","c1",700,500)
#c1.SetGrid()
c1.SetLogy()
#c1.SetLogx()
c1.SetBottomMargin(0.15)

mg=ROOT.TMultiGraph()
mgeps=ROOT.TMultiGraph()
graph_limit1=ROOT.TGraph(len(mass1),mass1,limit1)
graph_limit1.SetTitle("graph_limit1")
graph_limit1.SetMarkerSize(1)
graph_limit1.SetMarkerStyle(20)
graph_limit1.SetMarkerColor(kBlack)
graph_limit1.SetLineWidth(2)
graph_limit1.SetLineStyle(7)
graph_limit1.GetYaxis().SetRangeUser(0,100)
graph_limit1.GetXaxis().SetRangeUser(0.2,0.6)
graph_limit1.GetXaxis().SetMoreLogLabels()
graph_limit1.GetYaxis().SetTitle("#vareps")
graph_limit1.GetYaxis().SetTitleSize(2)
graph_limit1.GetXaxis().SetTitle("m(A') [GeV]")

graph_limit1obs=ROOT.TGraph(len(mass1),mass1,limit1obs)
graph_limit1.SetTitle("graph_limit1obs")
graph_limit1.SetMarkerSize(1)
graph_limit1.SetMarkerStyle(20)
graph_limit1.SetMarkerColor(kBlack)
graph_limit1.SetLineWidth(2)
graph_limit1.SetLineStyle(1)

graph_limit195up=ROOT.TGraphAsymmErrors(len(mass1),mass1,limit1,masserr1,masserr1,limit195up,limit195down)
graph_limit195up.SetTitle("graph_limit195up")
graph_limit195up.SetFillColor(ROOT.TColor.GetColor(252,241,15))

graph_limit168up=ROOT.TGraphAsymmErrors(len(mass1),mass1,limit1,masserr1,masserr1,limit168up,limit168down)
graph_limit168up.SetTitle("graph_limit168up")
graph_limit168up.SetFillColor(kGreen);
graph_limit168up.SetMarkerColor(kGreen)

graph_lhcb=ROOT.TGraph(len(lhcbmass),lhcbmass,lhcblimit)
graph_lhcb.SetTitle("graph_lhcb")
graph_lhcb.SetMarkerSize(0.001)
graph_lhcb.SetMarkerStyle(20)
graph_lhcb.SetMarkerColor(4)
graph_lhcb.SetLineWidth(2)
graph_lhcb.SetLineColor(4)
graph_lhcb.SetLineStyle(1)



mg.Add(graph_limit195up,"3")
mg.Add(graph_limit168up,"3")
mg.Add(graph_limit1,"pl")
mg.Add(graph_limit1obs,"pl")
mg.Add(graph_lhcb,"pl")

mg.GetXaxis().SetRangeUser(0.2,0.6)
mg.Draw("APC")

mg.GetYaxis().SetRangeUser(1e-4,1e-2)
mg.GetYaxis().SetTitle("#varepsilon")
mg.GetYaxis().SetTitleOffset(0.9)
mg.GetYaxis().SetTitleSize(0.05)
mg.GetXaxis().SetTitle("m(A') [GeV]")
mg.GetXaxis().SetTitleSize(0.05)
mg.GetXaxis().SetMoreLogLabels()
c1.Update()
legend=ROOT.TLegend(0.5,0.6,0.8,0.9)
cmsTag=ROOT.TLatex(0.13,0.917,"#scale[1.1]{CMS}")
cmsTag.SetNDC()
cmsTag.SetTextAlign(11)
#cmsTag.Draw()
cmsTag2=ROOT.TLatex(0.215,0.917,"#scale[0.825]{#bf{#it{Preliminary}}}")
cmsTag2.SetNDC()
cmsTag2.SetTextAlign(11)
#cmsTag.SetTextFont(61)
#cmsTag2.Draw()
cmsTag3=ROOT.TLatex(0.90,0.917,"#scale[0.9]{#bf{"+str(lumi_project)+" fb^{-1} (13 TeV)}}")
cmsTag3.SetNDC()
cmsTag3.SetTextAlign(31)
cmsTag.SetTextFont(61)
cmsTag3.Draw()
leg=ROOT.TLegend(0.2, 0.65,0.4, 0.85)  
leg.SetBorderSize( 0 )
leg.SetFillStyle( 1001 )
leg.SetFillColor(kWhite) 
leg.AddEntry( graph_limit1obs , "Observed",  "LP" )
leg.AddEntry( graph_limit1 , "Expected",  "LP" )
leg.AddEntry( graph_limit168up, "#pm 1#sigma",  "F" ) 
leg.AddEntry( graph_limit195up, "#pm 2#sigma",  "F" ) 
leg.Draw("same")
c1.SaveAs(combine_plots + "limiDarkPhoton_eps_run23.root")
c1.SaveAs(combine_plots + "limitDarkPhoton_eps_run23.pdf")
c1.SaveAs(combine_plots + "limitDarkPhoton_eps_run23.png")
c2=ROOT.TCanvas("c2","c2",700,500)
c2.SetLogy()
cmsTag.Draw()
cmsTag2.Draw()
cmsTag3.Draw()
mgeps.Draw("APC")
leg2=ROOT.TLegend(0.65, 0.65,0.8, 0.85)  
leg2.SetBorderSize( 0 )
leg2.SetFillStyle( 1001 )
leg2.SetFillColor(kWhite) 

leg2.Draw("same")
c2.SaveAs(combine_plots + "thep.root")
