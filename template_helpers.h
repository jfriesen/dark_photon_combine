#include <cmath>
using namespace RooFit;


// to keep together the names and plot properties for each template histogram
struct templateDescription{
    std::string name;
    Int_t color;

    std::string pdfName;
    std::string dataHistName;

    // stat uncty param name
    std::string systName;
    // stat uncty pdfs/datahist names
    std::string pdfUpName;
    std::string dataHistUpName;

    std::string pdfDownName;
    std::string dataHistDownName;

    std::string plotName;

    templateDescription(std::string n, Int_t c) : name(n), color(c){
        pdfName          = name;
        plotName         = name;
        dataHistName     = "rdh_" + name;

        systName         = "stat" + name;
        pdfUpName        = pdfName + "_" + systName + "Up";
        dataHistUpName   = dataHistName + "_statUp";
        pdfDownName      = pdfName + "_" + systName + "Down";
        dataHistDownName = dataHistName + "_statDown";

    }

};

struct templateHistogram{
    std::string name;
    TH1*         histTH1;
    RooDataHist* histData;
    RooHistPdf*  histPdf;
    RooKeysPdf*  keysPdf;

    RooDataHist* histUpData;
    RooHistPdf*  histUpPdf;

    RooDataHist* histDownData;
    RooHistPdf*  histDownPdf;

    templateDescription descript;

    templateHistogram(templateDescription d) : descript(d){}
};


std::pair<TH1F*, TH1F*> makeUpDownHistograms(const TH1* originalHist, templateDescription fTemplate){
    Int_t numBins = originalHist->GetNbinsX();
    

    TH1F* histUp = new TH1F(fTemplate.dataHistUpName.c_str(), "",
        numBins, originalHist->GetXaxis()->GetXmin(), originalHist->GetXaxis()->GetXmax()
    );

    TH1F* histDown = new TH1F(fTemplate.dataHistDownName.c_str(), "",
        numBins, originalHist->GetXaxis()->GetXmin(), originalHist->GetXaxis()->GetXmax()
    );

    Double_t binValue;
    Double_t binError;
    // Fill the histograms with bin+error and bin-error values
    for (int i = 1; i <= numBins; ++i) {
        binValue = originalHist->GetBinContent(i);
        binError = originalHist->GetBinError(i);

        histUp->SetBinContent(i, binValue + binError);
        histDown->SetBinContent(i, binValue - binError);
    }

    return {histUp, histDown};
}


// use a CB + Gaussian from previous dPh search for now?
RooAddPdf* makeSignalModelExample(RooRealVar* mass_obs, Double_t mass_val, std::string namePostfix = ""){
    TFile* f_ws = TFile::Open("resources/pdfs2018.root");
    // TFile* f_ws = TFile::Open("resources/pdfs2018_rebuilt.root");
    
    RooWorkspace* w = (RooWorkspace*)f_ws->Get("dpworkspace");
    // RooWorkspace* w = (RooWorkspace*)f_ws->Get("dpworkspace_new");
    w->loadSnapshot("calibrated");

    RooRealVar* alpha1          = new RooRealVar("alpha1", "alpha1", w->var("alpha1")->getVal());
    RooRealVar* alpha2          = new RooRealVar("alpha2", "alpha2", w->var("alpha2")->getVal());
    RooRealVar* n1              = new RooRealVar("n1", "n1", w->var("n1")->getVal());
    RooRealVar* frac_gau        = new RooRealVar("frac_gau", "frac_gau", w->var("frac_gau")->getVal());
    RooRealVar* gau_reso_scale  = new RooRealVar("gau_reso_scale", "gau_reso_scale", w->var("gau_reso_scale")->getVal());
    RooRealVar* res_rel_unc     = new RooRealVar("res_rel_unc", "res_rel_unc", w->var("res_rel_unc")->getVal());
    RooRealVar* res_rel         = new RooRealVar("res_rel", "res_rel", 0.013);

    alpha1->setConstant(true);
    alpha2->setConstant(true);
    n1->setConstant(true);
    frac_gau->setConstant(true);
    gau_reso_scale->setConstant(true);
    res_rel_unc->setConstant(true);
    res_rel->setConstant(true);

    RooRealVar *MH = new RooRealVar("MH","MH",mass_val,mass_val,mass_val);
    MH->setConstant(true);
    RooFormulaVar* res_CB = new RooFormulaVar("res_CB", "MH*res_rel", RooArgList(*MH, *res_rel));
    RooFormulaVar* res_gau = new RooFormulaVar("res_gau", "gau_reso_scale*MH*res_rel", RooArgList(*gau_reso_scale,*MH,*res_rel));
    RooDoubleCB* signalModel_CB = new RooDoubleCB("signalModel_CB", "signalModel_CB", *mass_obs, *MH, *res_CB, *alpha1, *n1, *alpha2, *n1);
    // RooCrystalBall * signalModel_CB = new RooCrystalBall ("signalModel_CB", "signalModel_CB", *mass_obs, *MH, *res_CB, *alpha1, *n1, *alpha2, *n1);
    RooGaussian* signalModel_gau = new RooGaussian("signalModel_gau", "signalModel_gau", *mass_obs, *MH, *res_gau);
    RooAddPdf* sig_model = new RooAddPdf("sig_model", "sig_model", RooArgList(*signalModel_CB, *signalModel_gau), RooArgList(*frac_gau));

    f_ws->Close();

    return sig_model;
}


// to keep together the name and selection for each template histogram
struct templateSel{
    std::string name;
    std::string selection;
};


void createHistograms(const char* inputFileName, const char* outputFileName, 
    Int_t nBins,
    Double_t massMin,
    Double_t massMax,
    std::string hist_var,
    std::vector<templateSel> selections,
    std::string baseSelection
    ) {
    // Open the input ROOT file
    TFile inputFile(inputFileName, "READ");
    if (!inputFile.IsOpen()) {
        std::cerr << "Error: Could not open input file " << inputFileName << std::endl;
        return;
    }

    // Get the TTree from the input file
    TTree* tree = dynamic_cast<TTree*>(inputFile.Get("tree/tree")); 
    if (!tree) {
        std::cerr << "Error: Could not retrieve TTree from the input file." << std::endl;
        inputFile.Close();
        return;
    }

    // Define branches and associated histograms
    // std::vector<TBranch*> brDecays;  // List of branches to cut on
    std::vector<TH1F*> histograms;         // Corresponding histograms

    // Add branches to cut on and create histograms
    for (const auto& tmplt : selections) {
        // brDecays.push_back(tree->GetBranch(br.GetName())); 
        std::string hist_name = tmplt.name;
        std::string hist_descript = "Histogram for " + tmplt.name;
        histograms.push_back(new TH1F(hist_name.c_str(), hist_descript.c_str(), nBins, massMin, massMax)); 
    }

    // set sumW2 for histograms
    // Write histograms to the output file
    for (size_t i = 0; i < histograms.size(); ++i) {
        histograms[i]->Sumw2();
    }

    gROOT->SetBatch(kTRUE);
    // Fill histograms
    for (size_t i = 0; i < selections.size(); ++i) {
        std::string selection = baseSelection + " && " + selections[i].selection;
        std::string draw_expr = hist_var + ">>" + histograms[i]->GetName();
        std::cout<< "Drawing expession=  "<< draw_expr << "  "<< "Selection expression=  "<< selection<< std::endl;
        tree->Draw(draw_expr.c_str(), selection.c_str());
    }
    

    // Open the output ROOT file
    TFile outputFile(outputFileName, "RECREATE");
    if (!outputFile.IsOpen()) {
        std::cerr << "Error: Could not open output file " << outputFileName << std::endl;
        return;
    }

    // Write histograms to the output file
    for (size_t i = 0; i < histograms.size(); ++i) {
        histograms[i]->Write();
        delete histograms[i];
    }

    // Close the output file
    outputFile.Close();
    // Close the input file
    inputFile.Close();
}

void mergeHistograms(const std::vector<std::string>& inputFiles, const std::string& outputFile) {
    // Create a new ROOT file for the merged histograms
    TFile* outputFilePtr = new TFile(outputFile.c_str(), "RECREATE");

    // Check if the output file is created successfully
    if (!outputFilePtr || outputFilePtr->IsZombie()) {
        std::cerr << "Error: Unable to create output file " << outputFile << std::endl;
        return;
    }

    // Loop over input files and merge histograms
    for (const auto& inputFile : inputFiles) {
        // Open the ROOT file
        TFile* inputFilePtr = TFile::Open(inputFile.c_str());

        // Check if the file is opened successfully
        if (!inputFilePtr || inputFilePtr->IsZombie()) {
            std::cerr << "Error: Unable to open input file " << inputFile << std::endl;
            continue; // Skip to the next file in case of an error
        }

        // Loop over histograms in the input file and merge them
        TIter nextkey(inputFilePtr->GetListOfKeys());
        TKey* key;
        while ((key = dynamic_cast<TKey*>(nextkey()))) {
            TObject* obj = key->ReadObj();
            if (obj->IsA()->InheritsFrom("TH1")) {
                TH1* inputHist = dynamic_cast<TH1*>(obj);

                // Merge the histogram into the output file
                TH1* mergedHist = dynamic_cast<TH1*>(outputFilePtr->Get(inputHist->GetName()));
                if (!mergedHist) {
                    mergedHist = static_cast<TH1*>(inputHist->Clone());
                    mergedHist->SetDirectory(outputFilePtr);
                } else {
                    mergedHist->Add(inputHist);
                }
            }
        }

        // Close the input file
        inputFilePtr->Close();
    }

    // Save the merged histograms to the output file
    outputFilePtr->Write();
    
    // Close the output file
    outputFilePtr->Close();
}






